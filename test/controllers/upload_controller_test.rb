require 'test_helper'

class UploadControllerTest < ActionController::TestCase
  test "should get background" do
    get :background
    assert_response :success
  end

  test "should get picture" do
    get :picture
    assert_response :success
  end

end
