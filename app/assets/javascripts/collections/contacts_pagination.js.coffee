class Studentconnect.Collections.ContactsPaginate extends Backbone.Collection
  url: '/users/contacts'
  model: Studentconnect.Models.Contact

  initialize: ->
    @total_pages = 0
    @request_params =
      page:         1
      per_page:     4
      search_query: ''

  parse: (resp) ->
    @total_pages = parseInt(resp.total_pages)
    resp.objects

  url_with_params: ->
    url     = @url
    params  = {}

    if _.keys(@request_params).length > 0
      params = _.map(@request_params, (value, key) -> if value then "#{ key }=#{ value }" )
      params = _.compact(params).join("&")
      url = "#{ url }?#{ params }"

    return url

  fetch_with_params: (options) ->
    options.url = @url_with_params() unless options.url
    @fetch(options)

  pageInfo: ()  ->
    info =
      total: @total_pages,
      page: @request_params.page,
      perPage: @request_params.per_page,
      pages: Math.ceil(@total_pages / @request_params.per_page),
      prev: false,
      next: false

    max = Math.min(@total_pages, @request_params.page * @request_params.per_page)

    if @total_pages == @request_params.page * @request_params.per_page
      max = @total_pages

    #info.range = [(@request_params.page - 1) * @request_params.per_page + 1, max]
    info.range = [1..info.pages]
  
    if @request_params.page > 1
      info.prev = @request_params.page - 1
  
    if @request_params.page < info.pages
      info.next = @request_params.page + 1
    info

  numberPage: (page) ->
    @request_params.page = page
    @fetch_with_params
      reset: true
  nextPage: () ->
    if !@pageInfo().next
      return false
    @request_params.page = @request_params.page + 1
    @fetch_with_params
      reset: true

  previousPage: () ->
    if !@pageInfo().prev
      return false
    @request_params.page = @request_params.page - 1
    @fetch_with_params
      reset: true

