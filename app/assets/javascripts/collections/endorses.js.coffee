class Studentconnect.Collections.PitchEndorses extends Backbone.Collection
  url: "/pitches/endorses"
  model: Studentconnect.Models.Endorse

  save: () ->
    Backbone.sync('update', @, url: @url, contentType: 'application/json', data: JSON.stringify(endorses: @toJSON()))
