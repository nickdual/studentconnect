
$(document).ready(function() {
    function setNanoHeight() {
         var actualHeight = window.innerHeight || $(window).height(),
         nanoHeight = actualHeight - $('#utility-bar').outerHeight() - $('.app-header').outerHeight() - $('#edit-panel h1').first().outerHeight();

        $('.nano').height(nanoHeight);
        $(".nano").nanoScroller({ alwaysVisible: true, preventPageScrolling: true});
        alert("nano:" + nanoHeight)


    }

    $(function () {
      setNanoHeight();
    });

    $(window).resize(setNanoHeight);

    $('#edit-panel .pitch-site-editor').resize(function () {
      setNanoHeight();
    });
});

