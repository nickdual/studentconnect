class Studentconnect.Models.Profile extends Backbone.Model
  validation:
    first_name:
      required: true
      msg: 'Please enter first name'
    last_name:
      required: true
      msg: 'Please enter last name'
    degree_program:
      required: true
      msg: 'Please enter degree program'
    major:
      required: true
      msg: 'Please enter major'
    college:
      required: true
      msg: 'Please enter university '
  urlRoot: "/users/profile"
  idAttribute: "_id"
  paramRoot: 'profile'
  defaults:
    first_name: ""
    last_name: ""
    degree_program: ""
    major: ""
    college: ""

  toJSON: ->
    if @paramRoot
      data = {}
      data[@paramRoot] = _.clone(@attributes);
      return data
    else
      return _.clone(@attributes)