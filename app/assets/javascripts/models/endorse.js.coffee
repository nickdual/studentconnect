class Studentconnect.Models.Endorse extends Backbone.Model
  urlRoot: "/contacts"

  add_endorse:(opts)->
    url = @url()+"/add_contact?type=endorse"
    options = {}
    options.contentType = 'application/json'
    options.url = url
    options.data = JSON.stringify(opts.data)
    options.success = opts.success if opts.success
    console.log(options)
    (@sync or Backbone.sync).call @, 'create', @, options

  approve:(opts)->
    url = "/pitches/approve"
    options = {}
    options.contentType = 'application/json'
    options.url = url
    options.data = JSON.stringify(opts.data)
    options.success = opts.success if opts.success
    console.log(options)
    (@sync or Backbone.sync).call @, 'create', @, options

