class Studentconnect.Models.Pitch extends Backbone.Model
  paramRoot: 'pitch'
  urlRoot: "/pitches"
  defaults:
    title: "Title"
    headline: "This is where your headline goes"
    link_title: "My Links"
    list_title: "Additional Lists Title"
    facebook_like_title: "LIKE THIS PITCH"
    share_title: "SHARE THIS PITCH WITH OTHERS"
    video_title: "My details"
    contact_btn_text: "Contact me"
    contact_mini_btn_text: "Get in Touch"
    background_url: ""
    body_title: "Site Manager"
    body_detail: "Tell your story here. Say why you got started, why you love it, how you're different from others in your market, etc. You want people to get your story right away. Be authentic. Explain it like you're talking to your mom."
    published: true
    facebook_like: true
    endorser: true
    statistic: true
    share: true
    facebook_share: true
    twitter_share: true
    linkedIn_share: true
    email_share: true
    video: true
    link: true
    list: true
    hide_small_button: false
    redirect_to_interested_url: false


#  validate: (attrs, options) ->
#    messages = []
#    valid = true
#    if attrs.body_title is ""
#      $(".pitch-text[data-name='body_title']").addClass('highlighted')
#      messages.push("Pitch headline is required")
#      valid = false
#    if attrs.headline is ""
#      $(".ps-header").addClass('highlighted')
#      $("#spotlightModal").show()
#      messages.push("Pitch headline is required")
#      valid = false
#
#    if attrs.body_detail is ""
#      $(".pitch-text[data-name='body_detail']").addClass('highlighted')
#      messages.push("Pithe headline is required")
#      valid = false
#    if attrs.link_title is ""
#      $(".pitch-text[data-name='link_title']").addClass('highlighted')
#      messages.push("Pithe headline is required")
#      valid = false
#    if attrs.facebook_like_title is ""
#      $(".pitch-text[data-name='facebook_like_title']").addClass('highlighted')
#      messages.push("Pithe headline is required")
#      valid = false
#    return valid



  update_published: (opts) ->
    url = '/pitches/update_published'
    options =
      url: url
      type: 'POST'
      data: "published=" + Boolean(opts.published) + "&&id=" + opts.id
    _.extend options, opts
    (@sync or Backbone.sync).call @, 'create', @, options

  toJSON: ->
    if @paramRoot
      data = {}
      data[@paramRoot] = _.clone(@attributes);
      return data
    else
      return _.clone(@attributes)

  parse: (response) ->
    if response
      if response.pitch
        response.pitch.id = response.pitch._id.$oid
        response._id

    return response

  check_url: (opts)  ->
    url = "/pitches/check_url"
    options = {}
    options.contentType = 'application/json'
    options.url = url
    options.data = JSON.stringify(opts.data)
    options.success = opts.success if opts.success
    options.error = opts.error if opts.error
    (@sync or Backbone.sync).call @, 'create', @, options


