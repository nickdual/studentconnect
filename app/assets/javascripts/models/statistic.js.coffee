class Studentconnect.Models.Statistic extends Backbone.Model
  urlRoot: "/pitches/statistic"
  defaults:
    title: "Where ?"
    number: "A+"
  toJSON: ->
    if @paramRoot
      data = {}
      data[@paramRoot] = _.clone(@attributes);
      return data
    else
      return _.clone(@attributes)
