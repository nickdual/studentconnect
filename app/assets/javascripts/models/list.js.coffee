class Studentconnect.Models.List extends Backbone.Model
  urlRoot: "/pitches/list"

  defaults:
    title: "Title"
    body: "Description"
  delete_photo:(opts)->
    url = '/pitches/list_delete_photo'
    options = {}
    options.contentType = 'application/json'
    options.url = url
    options.data = JSON.stringify(opts.data)
    options.success = opts.success if opts.success
    console.log(options)
    (@sync or Backbone.sync).call @, 'create', @, options

  toJSON: ->
    if @paramRoot
      data = {}
      data[@paramRoot] = _.clone(@attributes);
      return data
    else
      return _.clone(@attributes)

