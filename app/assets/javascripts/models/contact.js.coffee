class Studentconnect.Models.Contact extends Backbone.Model
  urlRoot: "/contacts"

  add_contact:(opts)->
    url = @url()+"/add_contact"
    options =
      url: url
      type: "POST"
      data: "contact=" + opts.contact
      dataType: 'json'
    _.extend options, opts
    (@sync or Backbone.sync).call @, 'create', @, options

  get_contacts:(opts)->
    console.log opts
    url = @url()+"/get_contacts"
    options =
      url: url
      type: "GET"
      dataType: 'json'
      data:"key_search=" + opts.key_search
    _.extend options, opts
    (@sync or Backbone.sync).call @, 'create', @, options

