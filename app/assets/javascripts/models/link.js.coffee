class Studentconnect.Models.Link extends Backbone.Model
  urlRoot: "/pitches/link"
  defaults:
    title: "Title"
    url: "Url"
  toJSON: ->
    if @paramRoot
      data = {}
      data[@paramRoot] = _.clone(@attributes);
      return data
    else
      return _.clone(@attributes)
