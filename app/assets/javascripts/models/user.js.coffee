class Studentconnect.Models.User extends Backbone.Model
  initialize: ->
    Backbone.Model.prototype.initialize.apply(@, arguments)
    _.bindAll(@, "mark_to_revert", "revert")
    @mark_to_revert()

  validation:
    first_name:
      required: true
      msg: 'Please enter first name'
    last_name:
      required: true
      msg: 'Please enter last name'
    degree_program:
      required: true
      msg: 'Please enter degree program'
    major:
      required: true
      msg: 'Please enter major'
    college:
      required: true
      msg: 'Please enter university '
  urlRoot: "/users"

  toJSON: ->
    if @paramRoot
      data = {}
      data[@paramRoot] = _.clone(@attributes);
      return data
    else
      return _.clone(@attributes)

  check_unique:(opts)->
    url = "/users/unique"
    options = {}
    options.contentType = 'application/json'
    options.url = url
    options.data = JSON.stringify(opts.data)
    options.error = opts.error if opts.error
    options.success = opts.success if opts.success
    console.log(options)
    (@sync or Backbone.sync).call @, 'create', @, options

  mark_to_revert: ->
    @_revertAttributes = _.clone @attributes

  revert: ->
    @set(@_revertAttributes, {silent : true}) if @_revertAttributes


