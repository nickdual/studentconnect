class Studentconnect.Routers.Pitches extends Backbone.Router
  routes:
    "profileMgr": "manager"
    "profile/edit/:id": "edit"
    "profile/new": "new"
    "profile/email": "pitches_endorsement"
    "promote": "pitches_promote"
    "profile/:id": "pitches_view"
    "responses": "responses"
    "responses/:id": "response_detail"
    "": "home"
  home: ->
    if window.location.pathname == "/"
      location.href = '/#profileMgr'


  manager: ->
    current_pitch = new Studentconnect.Models.Pitch()
    current_pitch.url = "/pitches.json"
    view = new Studentconnect.Views.PitchesManage({model: current_pitch})
    current_pitch.fetch({success: ->
      view.render()
    })

  index: () ->
    $("#main-nav").show()
    collection = new Studentconnect.Collections.Pitches()
    index = new Studentconnect.Views.PitchesIndex({collection: collection})
    collection.fetch()

  edit: (id) ->
    pitch = new Studentconnect.Models.Pitch()
    pitch.url = '/pitches/' + id + '.json'
    PitchEdit = new  Studentconnect.Views.PitchesEdit({model: pitch})
    pitch.fetch({success: ->
      PitchEdit.render()
    })

  new: ->
    pitch = new Studentconnect.Models.Pitch()
    PitchEdit = new  Studentconnect.Views.PitchesEdit({model: pitch})
    PitchEdit.render()

  pitches_view: (id) ->
    pitch = new Studentconnect.Models.Pitch()
    pitch.url = '/pitches/' + id + '.json'
    PitchView = new Studentconnect.Views.PitchesView({model: pitch, el: "#pitch_container", page_view: true})
    pitch.fetch({success: ->
      PitchView.render()

    })

  pitches_promote: ->
    current_pitch = new Studentconnect.Models.Pitch()
    view = new Studentconnect.Views.PromotionTool({model: current_pitch})
    current_pitch.fetch(data: {type: "share"},success: ->
      view.render()
    )


  pitches_endorsement:() ->
    PitchPromotion = new Studentconnect.Views.PitchesPromotion({template: 'endorsement'})
    PitchPromotion.render()

  responses: () ->
    endorses = new  Studentconnect.Collections.PitchEndorses()
    view_endorses = new Studentconnect.Views.PitchResponses({collection: endorses})
    endorses.url = '/pitches/endorses'
    endorses.fetch({reset: true},success: ->

    )
  response_detail: (id) ->
    endorse = new Studentconnect.Models.Endorse()
    view = new Studentconnect.Views.PitchResponse({model: endorse})
    endorse.url = "/contacts/endorse"
    endorse.fetch(data: {id: id},success: ->
      view.render_view()

    )


