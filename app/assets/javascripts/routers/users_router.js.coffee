class Studentconnect.Routers.Users extends Backbone.Router
  routes:
    "sign_in": "sign_in"
    "sign_up": "sign_up"
    "forgotpassword": "reset"
    "confirmation/new": "confirmation"

  confirmation: () ->
    unless Studentconnect.currentUser
      view = new Studentconnect.Views.Confirmation()
      view.render()
    else
      window.location.href = "/#profileMgr"

  sign_in: () ->
    unless Studentconnect.currentUser
      view = new Studentconnect.Views.Login()
      view.render()
    else
      window.location.href = "/#profileMgr"

  sign_up: () ->
    unless Studentconnect.currentUser
      view = new Studentconnect.Views.SignUp()
      view.render()
    else
      window.location.href = "/#profileMgr"
  reset: () ->
    unless Studentconnect.currentUser
      view = new Studentconnect.Views.ResetPassword()
    else
      window.location.href = "/#profileMgr"




