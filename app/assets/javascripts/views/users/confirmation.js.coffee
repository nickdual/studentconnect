class Studentconnect.Views.Confirmation extends Backbone.View
  el: "#users_page"
  template: JST['users/confirm_password']
  initialize: ->
    @model = new Studentconnect.Models.User()
  events:
    'click #confirm': 'confirm'
    'keypress #user_email' : 'enterLogin'

  render: ->
    @$el.html(@template({model: @model}))
    @validate()

  validate: ->
    @$("#form_confirm").validate
      errorClass: "error"
      rules:
        "user[email]":
          required: true


  enterLogin:(e) ->
    if (e.keyCode == 13)
      @confirm(e);

  confirm: (e) ->
    el = $(@el)
    e.preventDefault()
    if @$("#form_confirm").validate().form()
      email = @$('#user_email').val()
      el.find('#confirm').button('loading')
      @model.url = "/users/confirmation"
      @model.save({user: {email: email}},
        success: (userSession, response) ->
          el.find('#confirm').button('reset')
          window.location.href = "/users#sign_in"
        ,error: (userSession, response)->
          window.location.href = "/users#sign_in"
          el.find("#confirm_error").html()
          el.find('#confirm').button('reset')

      )


