class Studentconnect.Views.SignUp extends Backbone.View
  el: "#users_page"
  template: JST['users/sign_up']
  events:
   "click #sign_up": "save"
  initialize: ->
    @model = new Studentconnect.Models.User()

  render: ->
    @$el.html(@template({model: @model}))
    @validate()

  validate: ->
    @$("#form_sign_up").validate
      errorClass: "error"
      rules:
        "user[first_name]":
          required: true
        "user[last_name]":
          required: true
        "user[email]":
          required: true
        "user[password]":
          required: true
          maxlength: 256
          minlength: 8

  save: (e) ->
    target = $(e.currentTarget)
    self = @
    if @$("#form_sign_up").validate().form()
      target.val("Creating")
      email =  @$("#user_email").val()
      first_name = @$("#user_first_name").val()
      last_name = @$("#user_last_name").val()
      password = @$("#user_password").val()

      @model.set("password_confirmation", @$("#user_password").val())
      @model.save( {user: {email: email, password: password, password_confirmation: password, first_name: first_name, last_name: last_name}},{
        success: (userSession, response) ->
          if response.status == "200"
            $("#sign_up_success").show()

            Studentconnect.currentUser = new Studentconnect.Models.User(response.user)
            window.location.href = "/#profileMgr"
          else
            console.log(response.message)
            $("#sign_up_error").html(response.message).show()
            target.val("Save")

      },
        error: (userSession, response) ->
          console.log(response)
      )
    e.preventDefault()