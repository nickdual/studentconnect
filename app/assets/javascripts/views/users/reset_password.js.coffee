class Studentconnect.Views.ResetPassword extends Backbone.View
  el: "#users_page"
  template: JST['users/reset_password']
  events: ->
    "click #reset_password": "reset_password"
  initialize: ->
    @model = new Studentconnect.Models.User()
    @model.url = "/users/password"
    @render()
  render: ->
    @$el.html(@template())
    @validate()

  validate: ->
    @$("#form_reset_password").validate
      errorClass: "error"
      rules:
        "user[email]":
          required: true

  reset_password: (e) ->
    e.preventDefault()
    el = $(@el)
    el.find('#reset_password').button('loading')
    if @$("#form_reset_password").validate().form()
      email = @$("#user_email").val()

      @model.save({user: {email: email}},
        success: (userSession, response) ->
          if response.success == true
            window.location.href = "/users#sign_in"
          else if response.success == false && response.flag == 'not_email'
            el.find("#reset_password_error").html(response.errors).show()
            el.find('#reset_password').button('reset')
          else
            el.find('#reset_password').button('reset')

      )



