class Studentconnect.Views.PitchesHeader extends Backbone.View
  el: "#pitch_header"
  template: JST['pitches/header']
  events:
    "click #add_to_list": "list_profile"
  initialize:(opts) ->
    @model = opts.model

  render: ->
    @$el.html(@template(model: @model))
    @iphone_checkbox()
    @render_list_profile()
    $("#list_profile").resize( ->
      alert('resize')
    )

  iphone_checkbox:() ->
    obj = @
    if obj.model.get("published") == true
      $('#checkbox_public').prop('checked', true);
    $('#checkbox_public').iphoneStyle(
      checkedLabel: 'YES',
      uncheckedLabel: 'NO',
      containerRadius: -1,
      onChange: (elem, value) ->
        obj.model.set('published', value)
        if obj.model.get("_id").$oid
          obj.model.update_published({published: value, id: obj.model.get("_id").$oid });
    );

  render_list_profile: ->
    list = new Studentconnect.Views.PitchesEditor(model: @model)
    list.render()

  list_profile: (e) ->
    e.preventDefault()
    @$("#list_profile").toggleClass('hide')
    @repositionProfile()
  repositionProfile: ->
    actualHeight = window.innerHeight || $(window).height()
    nanoHeight = actualHeight - $('#utility-bar').outerHeight() - $('.app-header').outerHeight() - $('#edit-panel h1').first().outerHeight();
    $('.nano').height(nanoHeight);
    $(".nano").nanoScroller({ alwaysVisible: true, preventPageScrolling: true});

