class Studentconnect.Views.PitchesEditor extends Backbone.View
  el: "#list_profile"
  template: JST['pitches/editor']
  initialize: ->
    $(document).on 'click', (e) ->
      if e.target not @el and e.target.parent('#list_profile').length is 0
        @$el.hide()
    .bind(@)

  events:
    "click .inline-help": "help"
    "click .panel-toggle": "panel_toggle"
    "click .show-hide": "toggle_show_hide"
    "click .tool h2": "tool"
    "click .tool a.open": "tool_open"
    "click .tool.add-content ul li": "add_content"
    "click ul#gallery_bg li": "click_gallery_bg"
    "click .options a.collapse": "open_collapse"
    "click .private-toggle-checkbox" : 'private_toggle'
    "click .fb_share_img" : 'fb_share_on_off'
    "click .tw_share_img" : 'tw_share_on_off'
    "click .email_share_img" : 'email_share_on_off'
    "click .In_share_img" : 'In_share_on_off'
    "click .btn-color": 'set_button_color'
    "keyup #contact_mini_btn_text": 'change_contact_mini_btn_text'
    "keyup #contact_btn_text": 'change_contact_btn_text'
    'click #display-middle-buttons' : 'display_small_buttons'
    'click #redirect_to_interested_url' :'redirect_to_interested_url'


  initialize: ->
    @view = @options.view
    @view_profile = undefined


  render: ->
    @$el.html(@template({model: @model }))
    #@$(".nano").nanoScroller({ preventPageScrolling: true,  alwaysVisible: true })
    @$('#edit-panel .pitch-site-editor').resize( () ->
      #alert("nano")
    )
    @


  #click events
  help: (e) ->
    e.preventDefault()
  open_collapse: (e) ->
    e.preventDefault()
    target = $(e.currentTarget)

  add_content: (e) ->
    $("#spotlightModal").show()
    e.preventDefault()
    target = $(e.currentTarget)
    target.hide()
    status = target.attr('data-add-content')
    switch
      when status is "facebook-tool"  then  @model.set("facebook_like",true)
      #when status is "body-tool"  then  @model.set("body",true)
      when status is "products-tool"  then @model.set("list",true)
      when status is "shares-tool"  then @model.set("share",true)
      when status is "video-tool" then @model.set("video",true)
      when status is "links-tool" then @model.set("link",true)
      when status is "endorsements-tool" then @model.set("endorser",true)
      when status is "statistics-tool" then @model.set("statistic",true)

      else console.log('Something else')
    parent = $("#" + target.attr('data-add-content'))
    parent.show()
    id =  parent.find('h2').attr('data-spotlight')

  close_tool: (e) ->
    $("#spotlightModal").hide()
    e.preventDefault()
    parent = $(e.currentTarget).parent()[0]
    status = parent.id
    view_id_hidden = undefined
    switch
      when status is "facebook-tool"
        @model.set("facebook_like",false)
        view_id_hidden = "section#fblike"
      when status is "products-tool"
        @model.set("list",false)
        view_id_hidden = "section#list-pitch"
      when status is "shares-tool"
        view_id_hidden = "section#share"
        @model.set("share",false)
      when status is "video-tool"
        view_id_hidden = "section#video"
        @model.set("video",false)
      when status is "links-tool"
        view_id_hidden = "section#links"
        @model.set("link",false)
      when status is "endorsements-tool"
        view_id_hidden = "section#endorsements"
        @model.set("endorser",false)
      when status is "statistics-tool"
        view_id_hidden = "section#stats"
        @model.set("statistic",false)
      else console.log('Something else')
    id = "li[data-add-content='" + parent.id + "']"
    @$(id).show()
    $(parent).hide()
    id_spotlight =  $(parent).find('h2').attr('data-spotlight')
    @view.hide_tag(view_id_hidden) if view_id_hidden
    @view.remove_spotlight(id_spotlight) if id_spotlight

  tool: (e) ->
    e.preventDefault()
    target = $(e.currentTarget)
    parrent = target.closest(".tool")
    $(".highlighted").removeClass("highlighted")
    id = target.attr('data-spotlight')
    unless  $(id).css('display') is "none"
      $(id).addClass('highlighted')
      $("#spotlightModal").show()
      if $(id).length > 0
        offset = $(id).offset().top - 130
        $('html, body').animate({scrollTop: offset }, 'slow')
    @repositionProfile()
    if parrent.find(".workspace").length > 0
      if parrent.find(".workspace").css('display') is 'none'
        parrent.find(".workspace").show()
        parrent.find(".open").addClass('minus')

      else
        parrent.find(".workspace").hide()
        parrent.find(".open").removeClass('minus')


  tool_open: (e) ->
    e.preventDefault()
    target = $(e.currentTarget).closest(".tool")
    parrent = target.parent()
    $(".minus").removeClass("minus")
    $(".highlighted").removeClass("highlighted")
    if target.find(".workspace").css('display') is 'none'
      target.find(".workspace").show()
      target.find(".open").addClass('minus')
      id = target.find("h2").attr('data-spotlight')
      $("#spotlightModal").show()
    else
      target.find(".workspace").hide()
      target.find(".open").removeClass('minus')
      id = target.find("h2").attr('data-spotlight')
      $("#spotlightModal").hide()
    @repositionProfile()



  panel_toggle: (e) ->
    e.preventDefault()
    if @$(".lionscroll").hasClass('opened')
      @$(".lionscroll").removeClass('opened').addClass('closed')
      @$(".lionscroll").css('left', '-280px')
      $("#page-container").css('margin-left','0px')
    else
      @$(".lionscroll").removeClass('closed').addClass('opened')
      @$(".lionscroll").css('left', '0px')
      $("#page-container").css('margin-left','280px')

  click_gallery_bg: (e) ->
    e.preventDefault()

  private_toggle: (e)->
    if e.currentTarget.checked
      $(e.currentTarget).val('true')
      $('body').find('#li_button_header').find('.btn-privacy_invite').removeClass('hide_privacy')
      $('body').find('#li_button_header').find('.btn-privacy_promote').addClass('hide_privacy')
    else
      $('body').find('#li_button_header').find('.btn-privacy_invite').addClass('hide_privacy')
      $('body').find('#li_button_header').find('.btn-privacy_promote').removeClass('hide_privacy')
      $(e.currentTarget).val('false')

  fb_share_on_off:(e)->
    fb_check = $(e.currentTarget).siblings('.fb_check')
    if fb_check.css('display') == 'block'
      fb_check.css('display', 'none')
      $(e.currentTarget).siblings('.fb_share_value').val('false')
      $('body').find('.facebook_img').css('display', 'none')
    else
      fb_check.css('display', 'block')
      $(e.currentTarget).siblings('.fb_share_value').val('true')
      $('body').find('.facebook_img').css('display', 'block')
  tw_share_on_off:(e)->
    tw_check = $(e.currentTarget).siblings('.tw_check')
    if tw_check.css('display') == 'block'
      tw_check.css('display', 'none')
      $(e.currentTarget).siblings('.tw_share_value').val('false')
      $('body').find('.twitter_img').css('display', 'none')
    else
      tw_check.css('display', 'block')
      $(e.currentTarget).siblings('.tw_share_value').val('true')
      $('body').find('.twitter_img').css('display', 'block')

  email_share_on_off:(e)->
    email_check = $(e.currentTarget).siblings('.email_check')
    if email_check.css('display') == 'block'
      email_check.css('display', 'none')
      $(e.currentTarget).siblings('.email_share_value').val('false')
      $('body').find('.email_img').css('display', 'none')
    else
      email_check.css('display', 'block')
      $(e.currentTarget).siblings('.email_share_value').val('true')
      $('body').find('.email_img').css('display', 'block')

  In_share_on_off:(e)->
    In_check = $(e.currentTarget).siblings('.In_check')
    if In_check.css('display') == 'block'
      In_check.css('display', 'none')
      $(e.currentTarget).siblings('.In_share_value').val('false')
      $('body').find('.linkedin_img').css('display', 'none')
    else
      In_check.css('display', 'block')
      $(e.currentTarget).siblings('.In_share_value').val('true')
      $('body').find('.linkedin_img').css('display', 'block')

  set_button_color:(e)->
    $(e.currentTarget).parents('.button_list').find('label.checked').removeClass('checked')
    label_btn = $(e.currentTarget).find('label')
    label_btn.addClass('checked')
    color = label_btn.attr('data-color')
    $(e.currentTarget).siblings('#input_buttons_color').val(color)
    class_name = $('body').find('.cta-btn-view').removeClass('color-1 color-2 color-3 color-4')
    $('body').find('.cta-btn-view').addClass(color)
    @model.set("button_color",color)
    e.preventDefault()

  change_contact_btn_text:(e)->
    text = $(e.currentTarget).val()
    @model.set("contact_btn_text",text)

  change_contact_mini_btn_text:(e)->
    text = $(e.currentTarget).val()
    @model.set("contact_mini_btn_text",text)


  display_small_buttons:(e) ->
    if e.currentTarget.checked
      $(e.currentTarget).val('true')
      @model.set("hide_small_button",false)
      $("a.cta-mini").show()
    else
      @model.set("hide_small_button",true)
      $(e.currentTarget).val('false')
      $("a.cta-mini").hide()

  redirect_to_interested_url:(e)->
    if e.currentTarget.checked
      $(e.currentTarget).val('true')
      @model.set("redirect_to_interested_url",true)
    else
      @model.set("redirect_to_interested_url",false)
      $(e.currentTarget).val('false')

  toggle_show_hide: (e) ->
    e.preventDefault()
    target = $(e.currentTarget)
    $(".highlighted").removeClass("highlighted")
    if target.hasClass("close")
      target.removeClass("close")
      #$(target.attr('data-spotlight')).addClass("highlighted")
      $(target.attr('data-spotlight')).hide()
      @model.set(target.attr('data-name'),false)
    else
      target.addClass("close")
      $(target.attr('data-spotlight')).addClass("highlighted")
      $(target.attr('data-spotlight')).show()
      $("#spotlightModal").show()
      offset =  $(target.attr('data-spotlight')).offset().top - 130
      $('html, body').animate({scrollTop: offset }, 'slow')
      @model.set(target.attr('data-name'),true)


  repositionProfile: ->
    actualHeight = window.innerHeight || $(window).height()
    nanoHeight = actualHeight - $('#utility-bar').outerHeight() - $('.app-header').outerHeight() - $('#edit-panel h1').first().outerHeight();
    $('.nano').height(nanoHeight);
    $(".nano").nanoScroller({ alwaysVisible: true, preventPageScrolling: true});
