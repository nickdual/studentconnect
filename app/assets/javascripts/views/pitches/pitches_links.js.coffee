class Studentconnect.Views.PitchesLinks extends  Backbone.View
  el: "#links"
  template: JST['pitches/links']

  events:
    "click .links-add": "add_link"
    "click .remove-link": "close"
    "click .collapse": "collapse"
    "keyup .link-title": "update_title"
    "keyup .link-url": "update_url"


  initialize: (opts) ->
    @collection = opts.collection
    this.listenTo(@collection, 'reset', @addAll)
    this.listenTo(@collection, 'add', @addOne)
    @render()

  addOne: (model) ->
    view = new Studentconnect.Views.PitchLink({model: model})
    view.render()

  addAll: ->
    $("#links >> .link_ul").html('')
    @collection.forEach(@addOne, @)

  render: ->
    #@$el.html(@template())
    @addAll()

  add_link: (e) ->
    link_new = new Studentconnect.Models.Link()
    @collection.add(link_new)
    e.preventDefault()


  close: (e) ->
    e.preventDefault()
    target = $(e.currentTarget)
    cid = target.attr('cid')
    @collection.remove(cid)

  collapse: (e) ->
    e.preventDefault()
    target = $(e.currentTarget)
    parent = target.parent()

    if $(parent).find(".offering-edit").css('display') is 'none'
      $(parent).find(".offering-edit").show()
      $(parent).find("a.collapse").addClass('minus')
    else
      $(parent).find(".offering-edit").hide()
      $(parent).find("a.collapse").removeClass('minus')

  update_title: (e) ->
    focused = $(document.activeElement)
    cid = focused.closest('li')
    model = @collection.get(cid[0].id)
    model.set('title',focused.text())

  update_url: (e) ->
    focused = $(document.activeElement)
    cid = focused.closest('li')
    model = @collection.get(cid[0].id)
    model.set('url',focused.text())

