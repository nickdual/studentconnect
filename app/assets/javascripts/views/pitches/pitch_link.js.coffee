class Studentconnect.Views.PitchLink extends Backbone.View
  el: ".link_ul"
  template: JST['pitches/link_li_view']
  initialize: ->
    @model.on('remove', @destroy, @)

  render: ->
    console.log 'render pitch link'
    @$el.append(@template({model: @model}))
    @delegateEvents(@events)
    @initTooltip()
    @

  initTooltip:() ->
    @$("#view_link_title_#{@model.cid}").qtip(
      content:
        text: (event, api) ->
          return $(this).attr('qtip-content')
      ,
      position:
        my: 'bottom center'
        at: 'top center'
      style:
        classes: 'qtip qtip-default qtip-tooltip'
    );

    @$("#view_link_url_#{@model.cid}").qtip(
      content:
        text: (event, api) ->
          return $(this).attr('qtip-content')
      ,
      position:
        my: 'bottom center'
        at: 'top center'
      style:
        classes: 'qtip qtip-default qtip-tooltip'
    );

  validate: ->
    @$("#form_edit_link_#{@model.cid }").validate
      errorClass: "error"
      rules:
        "link[title]":
          required: true
        "link[url]":
          required: true


  destroy: ->
    console.log('remove model')
    $('body').find('.link_ul').find("li#" + @model.cid).remove()
