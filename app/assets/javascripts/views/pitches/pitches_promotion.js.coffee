class Studentconnect.Views.PitchesPromotion extends Backbone.View
  el: "#pitch_container_promotion"
  template: JST['pitches/promotion_email']


  initialize:(opts) ->
    @data = opts.data if opts.data

  events:
    'click .send_email' : 'sendEmail'
    'click li.li_option' : 'changeTemplateMessage'
    'click .recipient-bar' :'focusInputEmail'
    'click li.contact_search':'selectContactSearch'
    'click .delete': 'deleteContactTag'
    'keydown #typeaheadInput' : 'keydownTab'
    'click .select-all': 'selectAllContact'
    'click .contact_li': 'selectContact'
    'click #Browse':'viewContact'
    'click #add-contact-btn' :'addContact'

  render: ->
    @$el.html(@template({flag: @flag}))
    @searchContact()
    @changeTab()

  keydownTab:(e) ->
    if e.keyCode == 9 && $(e.currentTarget).val() != ''
      email_old = $('#contact_email').val()
      email_old = email_old.split(',')
      email = $(e.currentTarget).val()
      if @validateEmail(email)
        @addContactTag(email_old,email)
      else
        @$('.note_title').removeClass('hide')
        @$('.note_title').text('Please enter a valid email address')
        @$('.note_title').addClass('color_red')
        setTimeout(() ->
          @$('.note_title').addClass('hide');
        , 2000);
      $('#typeaheadInput').val('')
      $('#typeaheadInput').focus()
      e.preventDefault()

  validateEmail:(email) ->
    format = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/
    return format.test(email)

  addContactTag:(email_old, email, name = '',flag = null)->
    if email_old.indexOf(email) == -1
      email_old.push( email)
      email_new = email_old.join()
      @$('#contact_email').val(email_new)
      contact_model = new Studentconnect.Models.Contact({email: email, first_name: name})
      contact_tag = new Studentconnect.Views.ContactView(contact: contact_model)
      contact_tag.render_contact_tag()
    else
      @$('.note_title').removeClass('hide')
      if flag == null
        @$('.note_title').text('That contact has already been added')
        @$('.note_title').addClass('color_red')
      setTimeout(() ->
        @$('.note_title').addClass('hide');
      , 2000);

  removeContactTag:(email) ->
    email_old = @$('#contact_email').val()
    email_old = email_old.split(',')
    index = email_old.indexOf(email)
    email_old.splice(index,1)
    email_new = email_old.join()
    @$('#contact_email').val(email_new)
    @$('.selected-recipients').find('li[data-value="' + email + '"]').remove()

  selectContactSearch:(e) ->
    email = $(e.currentTarget).find('.contact_email').text()
    name = $(e.currentTarget).find('.contact_name').text().trim()
    email_old = $('#contact_email').val()
    email_old = email_old.split(',')
    @addContactTag(email_old,email,name)
    $(e.currentTarget).parents('.result-contact').css('display','none')
    $('#typeaheadInput').val('')
    @$('#typeaheadInput').focus()
    contact_on_list = @$('#contact_list').find('input[data-value="' + email + '"]')
    contact_on_list.prop('checked', true);
    contact_on_list.parent().addClass('selected')

  deleteContactTag:(e) ->
    email = $(e.currentTarget).siblings('.email_tags').text()
    @removeContactTag(email)
    li = @$('#contact_list').find('input[data-value="' + email + '"]')
    li.prop('checked', false)
    li.parent().removeClass('selected')
    e.preventDefault()



  searchContact:()->
    $('#typeaheadInput').db_autocomplete(callback: searchAutocomplete)
    $('#search_text_box').db_autocomplete(callback: searchContact)


  searchContact = (evt) ->
    value = $(evt).val()
    contacts = new Studentconnect.Collections.Contacts()
    contacts.fetch({
      data: {key_search: value },
      success: (data) ->
        if data.length > 0
          @$('.no-contacts ').hide()
          contacts_view = new Studentconnect.Views.Contacts({collection: data})
          contacts_view.render()
        else
          @$('#contact_list').html('')
          @$('.no-contacts ').show()
    })

  searchAutocomplete = (evt) ->
    value = $(evt).val()
    @contact = new Studentconnect.Models.Contact()
    @contact.get_contacts({key_search: value }).success (data) ->
      if data.length > 0
        top =  $(evt).offset().top + 30
        left =  $(evt).offset().left
        $('#result_contact').html('')
        _.each(data, (value) ->
          contact = new Studentconnect.Views.ContactView({contact: value})
          contact.render_contact_search()
        )
        $('.result-contact').css('display','block')
        $('.result-contact').offset({top: top, left: left})
      else
        $('.result-contact').css('display','none')



  sendEmail:(e) ->
    obj = @
    email =  @$('#contact_email').val()
    subject = @$('.subject_email').val()
    message = @$('.message').text()
    $(e.currentTarget).attr('value','Sending')
    $.ajax(
      type: 'POST'
      url: '/mailers/invite_email'
      data:
        "info[email]" : email
        "info[subject]" : subject
        "info[body]" : message

      success:()->
        obj.$('.email_input').val('')
        obj.$('.subject_email').val('')
        obj.$('.message').text('')
        $(e.currentTarget).attr('value','Send')
        @contact = new Studentconnect.Models.Contact()
        @contact.add_contact({contact:  email})
    )
    e.preventDefault()

  changeTemplateMessage:(e) ->
    li = $(e.currentTarget)
    value_li = li.attr('value')
    li.parents('.btn-select-template').find('span.value_default_template').text(li.text())
    subject =  @$el.find('.subject_email').attr('data-value-'+value_li)
    @$el.find('.subject_email').val(subject)
    message = @$el.find('textarea.message').attr('data-value-'+value_li)
    @$el.find('textarea.message').text(message)
    e.preventDefault()

  focusInputEmail:(e)->
    $(e.currentTarget).find('#typeaheadInput').focus()

  selectAllContact:(e) ->
    e.preventDefault()
    obj = @
    @$('#contact_list').find('input[type=checkbox]').prop('checked', true);
    @$('#contact_list').find('input[type=checkbox]').parent().addClass('selected')
    contact_selected = @$('#contact_list').find('input[type=checkbox]')
    contact_selected.each(() ->
      contact_li = $(this).parents('.contact_li')
      email = contact_li.find('.contact-email').text().trim()
      name = contact_li.find('.contact-name').text().trim()
      email_old = obj.$('#contact_email').val()
      email_old = email_old.split(',')
      obj.addContactTag(email_old, email, name, 'All')
    )

  selectContact:(e) ->
    email = @$(e.currentTarget).find('.contact-email').text()
    if $(e.currentTarget).find('input[type=checkbox]').is(':checked')
      $(e.currentTarget).find('input[type=checkbox]').prop('checked', false)
      $(e.currentTarget).removeClass('selected')
      @removeContactTag(email)
    else
      $(e.currentTarget).addClass('selected')
      $(e.currentTarget).find('input[type=checkbox]').prop('checked', true)
      name = @$(e.currentTarget).find('.contact-name').text().trim()
      email_old = @$('#contact_email').val()
      email_old = email_old.split(',')
      @addContactTag(email_old, email, name)
    e.preventDefault()

  viewContact:(e) ->
    @$('#advancedContactsSelector').show()
    $(e.currentTarget).remove()
    contacts = new Studentconnect.Collections.Contacts()
    contacts.fetch(
      success: (data) ->
        contacts_view = new Studentconnect.Views.Contacts({collection: data})
        contacts_view.render()
    )
  changeTab:(e) ->
    $('#myTab a').on('click', (e) ->
      e.preventDefault();
      $(this).tab('show');
    )


  addContact:(e) ->
    first_name  = @$('#contact_first_name').val()
    last_name  = @$('#contact_last_name').val()
    contact_email  = @$('#email_contact').val()
    university  = @$('#college_or_university').val()
    if @validateEmail(contact_email)
      contact =  new Studentconnect.Models.Contact({email:contact_email,first_name: first_name, last_name:last_name, university: university})
      contact.save({},
        success:(data) ->
          @$('#contact_first_name').val('')
          @$('#contact_last_name').val('')
          @$('#email_contact').val('')
          @$('.text-success').show()
          @$('#college_or_university').val('')
          setTimeout(() ->
            @$('.text-success').hide()
          , 2000);
        ,
        error:(e) ->
          @$('.text-fail').show()
          setTimeout(() ->
            @$('.text-fail').hide()
          , 2000);
      )
    else
      @$('.message_invalid').show()
      setTimeout(() ->
        @$('.message_invalid').hide()
      , 3000);