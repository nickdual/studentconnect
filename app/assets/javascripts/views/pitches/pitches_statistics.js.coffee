class Studentconnect.Views.PitchesStatistics extends  Backbone.View
  el: "#stats"
  template: JST['pitches/statistics']

  events:
    "click .statistics-add": "add_statistic"
    "click .remove-statistic": "close"
    "click .collapse": "collapse"
    "keyup .statistic-number": "update_number"
    "keyup .statistic-title": "update_title"


  initialize: (opts) ->
    @collection = opts.collection
    this.listenTo(@collection, 'reset', @addAll)
    this.listenTo(@collection, 'add', @addOne)
    @render()

  addOne: (model) ->
    view = new Studentconnect.Views.PitchStatistic({model: model})
    view.render()

  addAll: ->
    $("#stats >> .statistic_ul").html('')
    @collection.forEach(@addOne, @)

  render: ->
    #@$el.html(@template())
    @addAll()

  add_statistic: (e) ->
    statistic_new = new Studentconnect.Models.Statistic()
    @collection.add(statistic_new)
    e.preventDefault()


  close: (e) ->
    e.preventDefault()
    target = $(e.currentTarget)
    cid = target.attr('cid')
    $("article##{cid}").remove()
    @collection.remove(cid)

  collapse: (e) ->
    e.preventDefault()
    target = $(e.currentTarget)
    parent = target.parent()

    if $(parent).find(".offering-edit").css('display') is 'none'
      $(parent).find(".offering-edit").show()
      $(parent).find("a.collapse").addClass('minus')
    else
      $(parent).find(".offering-edit").hide()
      $(parent).find("a.collapse").removeClass('minus')

  update_title: (e) ->
    focused = $(document.activeElement)
    cid = focused.closest('li')
    model = @collection.get(cid[0].id)
    model.set('title',focused.text())

  update_number: (e) ->
    focused = $(document.activeElement)
    cid = focused.closest('li')
    model = @collection.get(cid[0].id)
    model.set('number',focused.text())
    console.log model
