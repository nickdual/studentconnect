class Studentconnect.Views.PromotionTool extends Backbone.View
  el: "#pitch_container_promotion"
  template: JST['pitches/promotion_tool']
  events:
    "click a.facebook": "post_fb"
  initialize:(opts) ->

  render: ->
    cur_path = "http://#{location.host}/#{@model.get('url')}"
    text = @model.get('headline')
    @$el.html(@template(url: cur_path, text: text))

  post_fb: (e) ->
    e.preventDefault()
    FB.ui({
      method: 'feed',
      name: 'Facebook Dialogs',
      link: "http://#{location.host}/#{@model.get('url')}",
      picture: '',
      caption: '',
      description: @model.get('headline')
    }, (response) ->
      if response && response.post_id
        console.log('Post was published.')
      else
        console.log('Post was not published.')


    )

