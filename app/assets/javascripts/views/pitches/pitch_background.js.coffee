class Studentconnect.Views.PitchesBackground extends Backbone.View
  el: "ul#gallery_bg"
  template: _.template('<li data-value="<%= obj.url %>">
    <span>
      <img src="<%= obj.thumb %>">
    </span>
    </li>')
  events:
    "click li": "change_bg"
  initialize: ->
    console.log(@collection)
    @pitch = @options.pitch
    console.log(@pitch)

  addOne: (obj) ->
    @$el.append(@template(obj))

  render: ->
    if @collection
      @collection.forEach(@addOne, @)
  change_bg: (e) ->
    target = $(e.currentTarget)
    url = target.attr("data-value")
    @pitch.set('background_url', url)



