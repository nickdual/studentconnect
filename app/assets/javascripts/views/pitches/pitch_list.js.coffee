class Studentconnect.Views.PitchList extends Backbone.View
  el: "#offerings-spotlight ul"
  template: JST['pitches/list_li_view']

  initialize:(option) ->

    @model.on('remove', @destroy, @)
    @index = option.index

  render: ->
    @$el.append(@template({model: @model}))
    @initUploadsPhoto()
    @initTooltip()
    @

  initTooltip: () ->
    @$("#view_list_title_#{@model.cid}").qtip(
      content:
        text: (event, api) ->
          return $(this).attr('qtip-content')
      ,
      position:
        my: 'bottom center'
        at: 'top center'
      style:
        classes: 'qtip qtip-default qtip-tooltip'
    )

    @$("#view_list_body_#{@model.cid}").qtip(
      content:
        text: (event, api) ->
          return $(this).attr('qtip-content')
      ,
      position:
        my: 'bottom center'
        at: 'top center'
      style:
        classes: 'qtip qtip-default qtip-tooltip'
    )

  destroy: ->
    console.log('remove model')
    @$("li#" + @model.cid).remove()
    $("#offerings-spotlight ul li##{@model.cid}").remove()

  initUploadsPhoto: (evt) =>
    self = @
    view_img = $("li##{@model.cid} img")
    photo_del = @$("#fileupload_list_image_#{@model.cid} .delete_photo_list")
    @$("#fileupload_list_image_#{@model.cid}").fileupload
      autoUpload: true
      url: '/uploads/photo'
      start: (e) ->

      added: (e, data) ->
        console.log(data)

      done: (e, data) ->
        console.log(data.result)
        console.log(data.result._id.$oid)
        self.model.set("photo_id",data.result._id.$oid )
        view_img.attr('src',data.result.thumb)
        photo_del.attr('data-id',data.result._id.$oid)
        photo_del.show()