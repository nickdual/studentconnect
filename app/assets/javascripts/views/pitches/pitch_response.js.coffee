class Studentconnect.Views.PitchResponse extends Backbone.View
  el: ".response-list"
  template: JST['pitches/response_li']
  template_view: JST['pitches/response_view']
  events:
    "click .carousel-control-right": "next"

  render: ->
    @$el.append(@template({model: @model}))

  render_view: ->
    $("#pitch_container").html(@template_view({model: @model}))





