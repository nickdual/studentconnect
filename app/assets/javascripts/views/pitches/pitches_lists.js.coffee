class Studentconnect.Views.PitchesLists extends  Backbone.View
  el: "#offerings-spotlight"
  template: JST['pitches/lists']
  initialize: ->
    #this.listenTo(@collection, 'add', @addOne)
    @collection.on('add',@addOne, @)
    @collection.on('reset', @addAll,@)
    @render()

  events:
    "click .list-add": "addList"
    "click .close-icon": "close"
    "click .collapse": "collapse"
    "click .delete_photo_list": "delete_photo"
    "keydown .list-title": "limit_title"
    "keyup .list-title": "update_title"
    "keyup .list-body": "update_body"

  addOne: (model) ->
    view = new  Studentconnect.Views.PitchList({model: model})
    view.render()

  addAll: ->
    @collection.forEach(@addOne, @)

  render: ->
    @


  addList: (e) ->
    e.preventDefault()
    list_new = new Studentconnect.Models.List()
    @collection.add(list_new)


  close: (e) ->
    e.preventDefault()
    target = $(e.currentTarget)
    cid = target.attr('cid')
    @collection.remove(cid)

  collapse: (e) ->
    e.preventDefault()
    target = $(e.currentTarget)
    parent = target.parent()

    if $(parent).find(".offering-edit").css('display') is 'none'
      $(parent).find(".offering-edit").show()
      $(parent).find("a.collapse").addClass('minus')
    else
      $(parent).find(".offering-edit").hide()
      $(parent).find("a.collapse").removeClass('minus')
  limit_title: (e) ->
    if e.keyCode == 13
      e.preventDefault()
  update_title: (e) ->
    if e.keyCode == 13
      e.preventDefault()
    else
      focused = $(document.activeElement)
      cid = focused.closest('li')
      model = @collection.get(cid[0].id)
      model.set('title',focused.text())

  update_body: (e) ->
    focused = $(document.activeElement)
    cid = focused.closest('li')
    model = @collection.get(cid[0].id)
    model.set('body',focused.text())


  delete_photo: (e) ->
    e.preventDefault()
    target = $(e.currentTarget)
    photo_del = $(target[0])
    photo_id = $(target[0]).attr("data-id")
    cid = $(target[0]).attr("data-cid")
    img = $("#fileupload_list_image_" + cid +  " img")
    view_img = $("li##{cid} img")
    model = @collection.get(cid)
    data = {}
    data.id = photo_id
    data.list_id = model.get("_id").$oid
    if photo_id
      model.delete_photo({data, success: ->
        $(img[0]).attr('src','/assets/placeholder-image2.gif')
        view_img.attr('src','/assets/placeholder-image2.gif')
        photo_del.hide()
        $(target[0]).attr("data-id", "")

      })




