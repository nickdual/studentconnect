class Studentconnect.Views.PitchEndorses extends Backbone.View
  el: "#endorsements_list"
  template: JST['pitches/endorses']
  events:
    "click .carousel-control-right": "next"
    "click .carousel-control-left": "pre"
    "click .carousel-indicators li a": "to"
    "click .show-hide": "show"
    "click .endorsement img": "show_detail"
    "click .endorsement .info": "show_detail"

  initialize: ->
    @collection.on('add',@addOne, @)
    @collection.on('reset', @addAll,@)
    @render()
    @page_number = 0
    @hide = true

  addOne: (model) ->

    index = _.indexOf(@collection.models, model)
    @page_number = Math.floor(index / 3)
    if index % 3 == 0
      view = new  Studentconnect.Views.PitchEndorse({ model: model, page: true, page_number: @page_number, index: index })
      #console.log("#{index}: #{true}: #{@page_number}")
    else
      view = new  Studentconnect.Views.PitchEndorse({el: "#endorsements_list ul.pages li.page[data-page='#{@page_number}'] ul", model: model, page: false, page_number: @page_number, index: index})
    #console.log(model)

    view.render()
    if @hide
      $("#endorsements span.count").html("#{@collection.models.length} Recommendations")
      $(".no-endorsements-private").hide()
      @hide = false
      if @collection.models.length <= 3
        $("#endorsements_list .bottom").hide()
        $("#endorsements_list .pager").hide()
      $('.carousel').carousel({

      })

  addAll: ->
    alert('addAlll')
    @collection.forEach(@addOne, @)

  render: ->
    @$el.html(@template({collection: @collection}))

    @

  next: () ->
    $('.carousel').carousel('next')

  pre: () ->
    $('.carousel').carousel('prev')
  to: (e) ->
    number = parseInt($(e.currentTarget).attr("data-slide-to"))

    $('.carousel').carousel(number)

  show: (e) ->
    e.preventDefault()
    target = $(e.currentTarget)
    cid = target.attr("data-id")
    model = @collection.get(cid)
    if target.hasClass("close")
      target.removeClass("close")
      model.set('show',false)
    else
      target.addClass("close")
      model.set('show',true)

  show_detail: (e) ->
    target = $(e.currentTarget).parent()

    $(".details").find(".photo img").attr("src",target.find("img").attr("src"))
    $(".details").find(".quote p").text(target.attr("data-quote"))
    $(".details").find(".quote small").text(target.find(".info .name").text())
    $(".details").toggleClass('hide')
    $("#endorsements_list").toggleClass('hide')
