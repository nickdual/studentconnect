class Studentconnect.Views.PitchesView extends Backbone.View
  el: "#pitch_view"
  template: JST['pitches/view']
  initialize:(opts) ->
    @el = opts.el
    @page_view = false
    @page_view = opts.page_view if opts.page_view

  events:
    "click a.endorse" : "endorse"
    "click a.manual": "form_endorse"
    "click #facebook_connect": "facebook_connect"
    "click #facebook-connect-button": "facebook_connect_button"
    "click #save_endorse": "save_endorse"
    "click #send_contact": "send_contact"
    "click .success .button": "close_modal"
    "click .details": "hide_endorsor_detail"
    "click a#contact_me": "contact_me"

  render: ->
    cur_path = "http://#{location.host}/#{@model.get('url')}"
    @$el.append(@template({model: @model, cur_path: cur_path, page_view: @page_view}))
    @initView()
    @validate_endorse()
    @validate_contact_me()
    @change_meta()
    @render_endorses()

  render_endorses: ->
    console.log('render_endorses')
    @endorses = new Studentconnect.Collections.PitchEndorses()
    view_endorses = new Studentconnect.Views.PitchEndorses({collection: @endorses})
    @endorses.url = '/pitches/endorses?pitch_id=' + @model.get('_id').$oid if @model.get('_id')
    @endorses.fetch(data: $.param({ approve: true, show: true}) ,success: (collection, data) ->
       if collection.models.length == 0
        $(".no-endorsements-public").show()
    )
  initView: ->
    if @page_view
      @$("a.endorse").show()
      @$(".no-endorsements-private").hide()
      @$(".no-endorsements-public").hide()
      @$("#endorsementModal").modal('hide')

  change_meta: ->
    $('meta[property=og\\:title]').attr('content',@model.get('title'))
    $('meta[property=og\\:image]').attr('content',"http://8a79d1eae94cb4f2cf60-ebd9519ab94bb23cc44fb615f97790bc.r88.cf2.rackcdn.com/pitch/uploads/share_info/snapshot/521dbde91e0b732934000001/thumb_friendster-huynh") #@model.get('image_url')
    $('meta[property=og\\:description]').attr('content',@model.get('headline'))

  validate_contact_me: ->
    @$("#form_contact").validate
      errorClass: "error"
      rules:
        "contact[first_name]":
          required: true
        "contact[last_name]":
          required: true
        "contact[email]":
          required: true
        "endorse[body]":
          required: true

  validate_endorse: ->
    @$("#form_endorse").validate
      errorClass: "error"
      rules:
        "contact[first_name]":
          required: true
        "contact[last_name]":
          required: true
        "contact[email]":
          required: true
        "endorse[body]":
          required: true
  close_modal: () ->
    @$("#endorsementModal").modal('hide')

  endorse: (e) ->
    self = @
    @$("#endorsementModal").modal('show')
      .on('hide', () ->
        self.reset_form_endorse()
      )

  contact_me: (e) ->
    e.preventDefault()
    self = @
    @$("#contact_me_modal").modal('show')
      .on('hide', () ->
        self.reset_form_contact()
      )

  form_endorse: (e) ->
    @$("#form_endorse").show()

  facebook_connect_button: () ->
    self = @
    FB.getLoginStatus( (response) =>
      if response.status is 'connected'
        FB.api('/me', (response) ->
          console.log(response)
          self.update_form_contact(response)
        )

      else if  response.status is 'not_authorized'
        @login()
      else
        @login()
    )

  facebook_connect: () ->
    self = @
    FB.getLoginStatus( (response) =>
      if response.status is 'connected'
        FB.api('/me', (response) ->
          console.log(response)
          self.update_form_endorse(response)
        )

      else if  response.status is 'not_authorized'
        @login()
      else
        @login()
    )

  login: ->
    self = @
    FB.login((response) ->
      if response.authResponse
        console.log(response)
        FB.api('/me', (response) ->
          console.log(response)
          self.update_form_endorse(response)
        )

    ,{scope: 'email,user_about_me'}

    )
  reset_form_contact: ->
    @$("#contact_me_modal #intro_contact").show()
    @$("#contact_me_modal #intro_success").hide()
    @$("#contact_me_modal .modal-body").show()
    @$(".facebook-connect").show()
  reset_form_endorse: ->
    @$(".facebook-connect").show()
    @$("#form_endorse").hide()
    @$("#contact_first_name").val("")
    @$("#contact_last_name").val("")
    @$("#contact_email").val("")
    @$("#contact_university").val("")
    @$("#endorse_success").hide()
    @$("#myModalLabel").show()
    @$("#title_success").hide()

  update_form_contact: (response) ->
    @$("#form_contact #contact_first_name").val(response.first_name)
    @$("#form_contact #contact_last_name").val(response.last_name)
    @$("#form_contact #contact_email").val(response.email)
    @$(".facebook-connect").hide()

  update_form_endorse: (response) ->
    @$("#contact_first_name").val(response.first_name)
    @$("#contact_last_name").val(response.last_name)
    @$("#contact_email").val(response.email)
    @$("#contact_university").val(_.last(response.education).school.name) if response.education
    @$(".facebook-connect").hide()
    @$("#form_endorse").show()

  save_endorse: (e) ->
    target = $(e.currentTarget)
    target.addClass('loading')
    self = $(@el)
    if @$("#form_endorse").validate().form()
      contact = new Studentconnect.Models.Contact()
      endorse = new Studentconnect.Models.Endorse()
      endorse.set("body",@$("#endorse_body").val())
      endorse.set("type","endorse")
      contact.set("first_name",@$("#contact_first_name").val())
      contact.set("last_name",@$("#contact_last_name").val())
      contact.set("university",@$("#contact_university").val())
      contact.set("email",@$("#contact_email").val())
      contact.set("phone",@$("#contact_phone").val())
      data = {}
      data.contact = contact.attributes
      data.endorse = endorse.attributes
      data.pitch_id = @model.get('_id').$oid
      endorse.add_endorse({data, success: (model, response) ->
        self.find("#form_endorse").hide()
        self.find("#endorse_success").show()
        self.find("#myModalLabel").hide()
        self.find("#title_success").show()
        self.find(".success .quote p").html("#{model.body}")
        self.find(".success .quote small").html("#{model.contact.first_name} #{model.contact.last_name}, #{model.contact.university} ")
        target.removeClass('loading')
      })
    else
      target.removeClass('loading')

  send_contact: (e) ->
    target = $(e.currentTarget)
    target.addClass('loading')
    self = $(@el)
    if @$("#form_contact").validate().form()
      self.find(".lead-name").text(@$("#form_contact #contact_first_name").val())
      contact = new Studentconnect.Models.Contact()
      endorse = new Studentconnect.Models.Endorse()
      endorse.set("body",@$("#form_contact #endorse_body").val())
      endorse.set("type","contact")
      contact.set("first_name",@$("#form_contact #contact_first_name").val())
      contact.set("last_name",@$("#form_contact #contact_last_name").val())
      contact.set("email",@$("#form_contact #contact_email").val())
      contact.set("phone",@$("#form_contact #contact_phone").val())
      data = {}
      data.contact = contact.attributes
      data.endorse = endorse.attributes
      data.pitch_id = @model.get('_id').$oid
      endorse.add_endorse({data, success: (model, response) ->
        target.removeClass('loading')
        self.find("#contact_me_modal #intro_contact").hide()
        self.find("#contact_me_modal #intro_success").show()
        self.find("#contact_me_modal .modal-body").hide()
      })
    else
      target.removeClass('loading')

  hide_endorsor_detail: (e) ->
    $(".details").toggleClass('hide')
    $("#endorsements_list").toggleClass('hide')






