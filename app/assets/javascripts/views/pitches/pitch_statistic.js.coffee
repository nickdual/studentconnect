class Studentconnect.Views.PitchStatistic extends Backbone.View
  el: ".row.statistic_ul"
  template: JST['pitches/statistic_li_view']
  initialize: ->
    @model.on('remove', @destroy, @)

  render: ->
    console.log 'render pitch statistic'
    @$el.append(@template({model: @model}))
    @delegateEvents(@events)
    @initTooltip()
    @

  initTooltip:() ->
    @$("#view_statistic_title_#{@model.cid}").qtip(
      content:
        text: (event, api) ->
          return $(this).attr('qtip-content')
      ,
      position:
        my: 'bottom center'
        at: 'top center'
      style:
        classes: 'qtip qtip-default qtip-tooltip'
    );

    @$("#view_statistic_url_#{@model.cid}").qtip(
      content:
        text: (event, api) ->
          return $(this).attr('qtip-content')
      ,
      position:
        my: 'bottom center'
        at: 'top center'
      style:
        classes: 'qtip qtip-default qtip-tooltip'
    );

  validate: ->
    @$("#form_edit_statistic_#{@model.cid }").validate
      errorClass: "error"
      rules:
        "statistic[title]":
          required: true
        "statistic[number]":
          required: true


  destroy: ->
    console.log('remove model')
    $('body').find('.statistic_ul').find("li#" + @model.cid).remove()
