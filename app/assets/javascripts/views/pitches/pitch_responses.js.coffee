class Studentconnect.Views.PitchResponses extends Backbone.View
  el: "#pitch_container"
  template: JST['pitches/responses']
  initialize: ->
    @collection.on('reset', @addAll,@)
    @render()

  events:
    "click a.approve": "approve"
    "click span.view_contact": "view_contact"

  addOne: (model) ->
    view = new Studentconnect.Views.PitchResponse({model: model})
    view.render()

  addAll: ->
    if @collection.models.length == 0
      @$(".response-list li.response").show()
    @collection.forEach(@addOne, @)

  render: ->
    @$el.html(@template({collection: @collection}))

    @

  view_contact: (e) ->
    target = $(e.currentTarget)
    id = target.attr("data-id")
    target.qtip(
      overwrite: false


      content:
        text: 'Loading...'
        ajax:
          url: '/users/contact'
          type: 'GET'
          data: {id: id}
          dataType: 'json'
          success: (data, status) ->
            console.log(data.first_name)
            content = '<div class="ui-tooltip-content">
               <span>' + data.first_name + ' ' + data.last_name + '</span>
               <p>
                  <strong>University:</strong>
                  ' + data.university + '
                  <br>
                  <strong>Phone:</strong>
                  ' + data.phone + '
                  <br>
                  <strong>Email:</strong>
                    <a style="color: #D95C1A;">' + data.email + '</a>
                  <br>
                </p>
            </div>'
            this.set('content.text', content)
      position:
        my: 'bottom center'
        at: 'top center'
      style:
        classes: 'qtip qtip-default profile-badge qtip-tooltip '
    ).qtip('show')
  approve: (e) ->
    e.preventDefault()
    cid = $(e.currentTarget).attr("data-id")
    @model = @collection.get(cid)
    data = {id: @model.get('_id').$oid }
    @model.approve({data, success: () ->
      $(e.currentTarget).html('<i class="icon-ok-sign"></i> Approved').addClass("disabled")
    })
