class Studentconnect.Views.PitchEndorse extends Backbone.View
  el: "#endorsements_list ul.pages"
  template: JST['pitches/endorse_li_view']


  initialize:(options) ->
    @page = options.page
    @page_number = options.page_number
    @index = options.index
    #console.log(@el)
  render: ->
    @$el.append(@template({model: @model, page: @page, page_number: @page_number, index: @index}))
    $(".pager ul").append("<li> <a data-target='#myCarousel' data-slide-to='" + @page_number + "'> </a></li>") if @page
    #console.log(@template({model: @model, page: @page, page_number: @page_number, index: @index}))
    @
