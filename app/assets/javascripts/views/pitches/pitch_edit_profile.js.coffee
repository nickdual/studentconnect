class Studentconnect.Views.PitchesEditProfile extends Backbone.View
  el: "#contact-info"
  template: JST['pitches/edit_profile']
  events:
    "keyup .profile-text": "profile_text"
    "keydown .profile-text": "limit_text"
    "blur .profile-text": "validation"
  initialize: ->
    console.log(@model)
    Backbone.Validation.bind(this)
    @model.bind("change:first_name", @unique, @)
    @model.bind("change:last_name", @unique, @)
    @model.bind('validated:invalid', (model, errors) ->
      console.log errors
      text = ""
      $.each(errors, (k, v) ->
         text += "<p>#{v}</p>"

      )
      $("#spotlightModal").show()
      offset =  $("#contact-info-spotlight").offset().top - 130
      $('html, body').animate({scrollTop: offset }, 'slow')
      $("#contact-info-spotlight").addClass("highlighted")
      $("#contact-info-spotlight").qtip(
        overwrite: false,
        show:
          event: false
        hide:
          event: false
        content:
          text
        ,
        position:
          my: 'top center'
          at: 'bottom center'
        style:
          classes: 'qtip qtip-default qtip-custom qtip-tooltip'
      ).qtip('show')
       .qtip('option', 'content.text', text)
    )

    @model.bind('validated:valid', (model, errors) ->
      $("#contact-info-spotlight").qtip("hide")

    )

  render: ->
    @$el.html(@template({model: @model}))
    @
  validation: (e) ->
    target = $(e.currentTarget)

    if target.text().trim() == ""
      target.html(target.text().trim())
      target.attr("data-placeholder", target.attr("data-placement"))
      target.css('min-width', "160px") if target.attr("data-name") is "degree_program"
      target.css('min-width', "60px") if target.attr("data-name") is "major"
      target.css('min-width', "100px") if target.attr("data-name") is "college"
    else
      target.css('min-width', "")
      target.attr("data-placeholder", "")

  limit_text: (e) ->
    if e.keyCode == 13
      e.preventDefault()

  profile_text: (e) ->
    if e.keyCode == 13
      e.preventDefault()
      return true
    else
      focused = $(document.activeElement)
      @model.set(focused.attr("data-name"), focused.text())

  unique: ->
    text = ""
    text += "<p>Please enter first name</p>" if @model.get('first_name') == ""
    text += "<p>Please enter last name</p>"  if @model.get('last_name') == ""
    if text
      $("#contact-info-spotlight").qtip(
        overwrite: false,
        show:
          event: false
        hide:
          event: false
        content:
          "text"
        ,
        position:
          my: 'top center'
          at: 'bottom center'
        style:
          classes: 'qtip qtip-default qtip-custom qtip-tooltip'
      ).qtip('show')
        .qtip('option', 'content.text', text)
    else
      $("#contact-info-spotlight").qtip("hide")








