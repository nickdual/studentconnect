class Studentconnect.Views.ContactsPagination extends  Backbone.View
  el: "#contact"
  template: JST['contacts/tr_contact']
  initialize: (opts) ->
    @collection.bind('reset',@addAll, @)
    #this.listenTo(@collection, 'reset', @addAll)
    #this.listenTo(@collection, 'add', @addOne)

  events:
    'click a.page_number': 'page_number'
    'click a.prev': 'previous'
    'click a.next': 'next'
    'click table#contacts_list tbody tr': 'show_contact'

  addOne: (model) ->
    console.log(model)
    @$("table#contacts_list tbody").append(@template({model: model}))

  addAll: ->
    if @collection.models.length == 0
      @$("table#contacts_list").hide()
    else
      @$("table#contacts_list").show()
    @$("table#contacts_list tbody").html('')
    @collection.forEach(@addOne, @)
    @$("#paginator").html(JST['helper/page_info'](@collection.pageInfo()))

  render: ->
    @addAll()

  page_number: (e) ->
    e.preventDefault()
    page = parseInt($(e.currentTarget).text())
    @collection.numberPage(page)

  previous: (e) ->
    e.preventDefault()
    @collection.previousPage()

  next: (e) ->
    e.preventDefault()
    @collection.nextPage()

  show_contact: (e) ->
    cid = $(e.currentTarget).attr("id")
    console.log(cid)
    model = @collection.get(cid)
    $("#show_contact").find(".modal-body .contact_date").html($.datepicker.formatDate('mm-dd-yy', new Date(model.get('created_at'))))
    $("#show_contact").find(".modal-body .contact_from").html(model.get('contact').first_name + ' ' + model.get('contact').last_name )
    $("#show_contact").find(".modal-body .contact_message").html(model.get('body'))
    $("#show_contact").find(".modal-body .contact_email").html(model.get('contact').email)
    $("#show_contact").find(".modal-body .contact_phone").html(model.get('contact').phone)
    $("#show_contact").modal('show')

