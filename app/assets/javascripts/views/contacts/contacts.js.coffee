class Studentconnect.Views.Contacts extends  Backbone.View

  initialize: (opts) ->
    @collection = opts.collection
    this.listenTo(@collection, 'reset', @addAll)
    this.listenTo(@collection, 'add', @addOne)
    @render()

  addOne: (model) ->
    view = new Studentconnect.Views.ContactView({model: model})
    view.render_contact_list()

  addAll: ->
    $("#contact_list").html('')
    @collection.forEach(@addOne, @)

  render: ->
    @addAll()

