class Studentconnect.Views.ContactView extends Backbone.View

  template_contact_search: JST['contacts/li_contact_search']
  template_contact_tag: JST['contacts/li_contact_tag']
  template_contact_list: JST['contacts/li_contact_list']

  initialize:(opts) ->
    @contact = opts.contact if opts.contact
    @model = opts.model if opts.model
  render_contact_search: ->
    @$el = $(@template_contact_search({contact: @contact})).appendTo('#result_contact')

  render_contact_tag: ->
    @$el = $(@template_contact_tag({contact: @contact})).appendTo('.selected-recipients')

  render_contact_list: ->
    @$el = $(@template_contact_list({contact: @model})).appendTo('#contact_list')