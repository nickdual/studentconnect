class Studentconnect.Views.PitchViewEdit extends Backbone.View
  el: "#pitch_view"
  template: JST['pitches/view_edit']

  initialize:(opts) ->
    @el = opts.el
    @profile = opts.profile
    @model.on("change:background_url" , @update_backgroud,@)
    @model.on("change:body_detail" , @update_display,@)
    @model.on("change:contact_btn_text" , @update_contact_button,@)
  events:
    #"mouseenter .hover_highlight" : 'onHighlight'
    #"mouseleave .hover_highlight" : 'offHighlight'
    "click #share li span.show-hide": "check_share"
    "keydown .ps-header": "limit_text"
    "keydown .pitch-text": "limit_text"
    "keyup .ps-header": "change_headline"
    "keyup .pitch-text": "pitch_text"
    "keyup .video-url": "change_video"
    "keydown .video-url": "limit_text"
    "blur .pitch-text": "validation"
    "blur .ps-header": "validation"
    "blur .video-url": "validation"
    "click .details": "hide_endorsor_detail"

  render: ->
    cur_path = "http://#{location.host}/#{@model.get('url')}"
    @$el.append(@template({model: @model, cur_path: cur_path}))
    @initProfile()
    @initUploadsPicture()
    @initPopoverUpload()
    @delegateEvents(@events)
    @initTooltip()
    @change_meta()
    @

  change_meta: () ->
    $('meta[property=og\\:title]').attr('content',@model.get('title'))
    $('meta[property=og\\:image]').attr('content', '')
    $('meta[property=og\\:description]').attr('content',@model.get('headline'))

  initProfile: ->
    @view_profile = new Studentconnect.Views.PitchesEditProfile({model: @profile})
    @view_profile.render()

  validation: (e) ->
    console.log(e)
    target = $(e.currentTarget)
    if target.text().trim() == ""
      target.attr("data-placeholder", target.attr("data-placement"))
    else
      target.attr("data-placeholder", "")

  update_contact_button: ->
    @$("a#contact_btn_bottom").html(@model.get("contact_btn_text"))
    @$("a#contact_btn_top").html(@model.get("contact_btn_text"))

  change_video: (e) ->
    focused = $(document.activeElement)
    video_youtube = focused.text()
    video_url = ''

    if video_youtube != ''
      if index_of_v_url = video_youtube.indexOf('v') > 0
        index_of_v_url = video_youtube.indexOf('v=')
        if index_of_v_url > 0
          video_id = video_youtube.substring(index_of_v_url + 2)
          text_url = 'http://www.youtube.com/v/'
          video_url = text_url.concat(video_id)
        else
          video_url = focused.text()
        if @model.get('video_url') != video_url
          @model.set("video_url",video_url)
          html = '<embed id="video_embed" src="' + video_url  + '"type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" style="width: 305px;height: 146px"> </embed><img id="image-no-video" src="' + Studentconnect.data.video_placeholder + '" class="hide">'
          video_wrapper = $("body").find("#video_wrapper")
          video_wrapper.html(html)
      else
        @model.set("video_url",video_url)
        @$('#video_embed').hide()
        @$('#image-no-video').show()

  check_charcount: (inputclass, max, e) ->
    focused = $(document.activeElement)
    console.log(focused.text().length)
    if focused.hasClass(inputclass) and e.which != 8 and focused.text().length >= max
      e.preventDefault();

  change_headline: (e) ->
    focused = $(document.activeElement)
    @model.set("headline", focused.text())
    console.log(@model.get("headline"))

  limit_text: (e) ->
    focused = $(document.activeElement)
    focused.removeClass("highlighted")
    if e.keyCode == 13
      e.preventDefault()
    else

      max = parseInt(focused.attr("data-max-chars"))
      @check_charcount("ps-header", max, e)


  check_share: (e) ->
    e.preventDefault()
    target = $(e.currentTarget).closest('li')
    span = target.find("span")
    if span.hasClass("close")
      span.removeClass("close")
      @model.set(target.attr('data-name'),true)
    else
      span.addClass("close")
      @model.set(target.attr('data-name'),false)

  pitch_text: (e) ->
    if e.keyCode == 13
      e.preventDefault()
      return true
    else
      focused = $(document.activeElement)
      max = parseInt(focused.attr("data-max-chars"))
      class_name = 'pitch-text'
      @check_charcount(class_name, max, e)
      @model.set(focused.attr("data-name"), focused.text())
    console.log(@model.get(focused.attr("data-name")))

  onHighlight:(e) ->
    @$('#spotlightModal').show()

  offHighlight:(e) ->
    @$('#spotlightModal').hide()

  initTooltip:() ->
    $(".top-qtip.get_tooltip").qtip(
      content:
        text: (event, api) ->
          return $(this).attr('qtip-content')
      ,
      position:
        my: 'bottom center'
        at: 'top center'

      style:
        classes: 'qtip qtip-default qtip-custom qtip-tooltip '
    );
    $(".left-qtip.get_tooltip").qtip(
      content:
        text: (event, api) ->
          return $(this).attr('qtip-content')
      ,
      position:
        my: 'bottom left'
        at: 'top left'
      style:
        classes: 'qtip qtip-default qtip-custom qtip-tooltip qtip-edit-video'
    );


  update_display:(e) ->
    @$('#view_pitch_body_detail').text(@model.get('body_detail'))

  initUploadsPicture: (evt) =>
    self = @$el
    $("#fileupload_picture").fileupload
      autoUpload: true
      url: '/uploads/picture'
      start: (e) ->

      added: (e, data) ->
        console.log(data)

      done: (e, data) ->
        console.log(data)
        Studentconnect.currentUser.set('current_picture',data.result.thumb)
        self.find(".avatar").css('background-image',"url(#{data.result.thumb})")

  update_backgroud:  ->
    @$('header').css("background-image","url(#{@model.get('background_url')})")

  initPopoverUpload: ->
    @$el.find('.btn_upload_bg').qtip(
      show: 'click'
      hide: 'unfocus'
      adjust:
        screen : true

      position:
        my: 'center'
        at: 'center'
        target: $('header')
      content:
        text: '
                                           <div class="upload">
                                              <form id="fileupload_background"  method="post" accept-charset="UTF-8">
                                                          <span class="upload-btn">
                                                              <input class="custom-bg-button" type="button" value="Upload an Image">
                                                              <input id="background_image" name="background[image]" type="file">
                                                          </span>
                                                          <span class="details">
                                                            JPG, GIF, or PNG.
                                                            <br>
                                                            Maximum file size is 5MB.
                                                            <br>
                                                            Dimension: 2,560 x 800 pixels.
                                                            <br>
                                                            Images with different dimension
                                                            <br>
                                                            will be resized.
                                                          </span>
                                              </form>

                                           </div>

       <div class="options image-gallery">
                    <div class="gallery backgrounds tab-panel">
                        <label>Gallery</label>
                        <ul id="gallery_bg">

                        </ul>
                    </div>

                </div>                                        ',
      style:
        classes: 'qtip qtip-default qtip-pos-rt qtip-focus qtip-custom qtip-popover qtip-upload-image-offer'
        tip:
          width: 8
          height: 8
      events:
        render: @initUploads

    )

  initUploads: (evt) =>

    background_defaults_view = new Studentconnect.Views.PitchesBackground({collection: Studentconnect.background_default, pitch: @model})
    background_defaults_view.render()
    self = @
    $("#fileupload_background").fileupload
      autoUpload: true
      url: '/uploads/background'
      start: (e) ->

      added: (e, data) ->
        console.log(data)
      done: (e, data) ->
        self.model.set('background_url',data.result.url)


  update_href:(e) ->
#    code update href here

  hide_endorsor_detail: (e) ->
    $(".details").toggleClass('hide')
    $("#endorsements_list").toggleClass('hide')





