class Studentconnect.Views.PitchesEdit extends Backbone.View
  el: "#pitch_container"
  template: JST['pitches/edit']
  events:
    "click .save": "save_change"
    "click .revert": "revert"

  initialize: ->
    @header = undefined
    @view = undefined
    @profile = Studentconnect.currentUser

    @links = new Studentconnect.Collections.PitchLinks()
    @statistics = new Studentconnect.Collections.PitchStatistics()
    @lists = new Studentconnect.Collections.PitchLists()
    @endorses = new Studentconnect.Collections.PitchEndorses()
    Backbone.Validation.bind(this)



  render: ->

    @$el.html("")
    @$el.append(@template())
    @render_header()
    @render_view()
    @render_endorses()
    @render_links(@links)
    @render_statistics(@statistics)
    @render_lists()
    @

  revert: (e) ->
    e.preventDefault()
    @links = new Studentconnect.Collections.PitchLinks()
    @statistics = new Studentconnect.Collections.PitchStatistics()
    @lists = new Studentconnect.Collections.PitchLists()

    @profile.revert()
    @profile.mark_to_revert()
    @model.fetch({success: =>
      @render()
    })


  render_endorses: ->
    console.log('render_endorses')
    self = @
    view_endorses = new Studentconnect.Views.PitchEndorses({collection: @endorses})
    @endorses.url = '/pitches/endorses?pitch_id=' + @model.get('_id').$oid if @model.get('_id')
    @endorses.fetch({ data: $.param({ approve: true}) },success: ->
    )

  render_lists: () ->
    self = @
    view_lists = new Studentconnect.Views.PitchesLists({collection: @lists})
    @lists.url = '/pitches/lists?pitch_id=' + @model.get('_id').$oid if @model.get('_id')
    @lists.fetch({reset: true}, success:(data) ->
      if data.length == 0
        list_new = new Studentconnect.Models.List()
        self.lists.add(list_new)
    )

  render_links:(@links) ->
    view_links = new Studentconnect.Views.PitchesLinks({collection: @links})
    id = ''
    id = @model.get('_id').$oid if @model.get('_id')
    @links.fetch(data: {pitch_id: id})

  render_statistics:(@statistics) ->
    self = @
    view_statistics = new Studentconnect.Views.PitchesStatistics({collection: @statistics})
    id = ''
    id = @model.get('_id').$oid if @model.get('_id')
    @statistics.fetch(data: {pitch_id: id},success:(data) ->
      if data.length == 0
        statistic_new = new Studentconnect.Models.Statistic()
        self.statistics.add(statistic_new)
    )

  render_header: ->
    @header = new Studentconnect.Views.PitchesHeader(model: @model)
    @header.render()

  render_editor: ->
    @editor = new Studentconnect.Views.PitchesEditor({model: @model ,view: @view})
    @editor.render()

  render_view: ->
    @view = new Studentconnect.Views.PitchViewEdit({model: @model, profile: @profile})
    @view.render()



  validate_links: ->
    result = true
    console.log(@links)
    if @links.length > 0
      @links.forEach((model) ->
        unless $("#form_edit_link_#{model.cid}").validate().form()
          result = false

      )
    return result

  save_change: (e) ->
    btn_save_change = $(e.currentTarget)
    @profile.url = "/users/profile.json"
    btn_save_change.val('Saving...')
    edit = true
    if @model.get('_id')
      edit = false
      @model.id = @model.get('_id').$oid
      @links.url = '/pitches/update_links?pitch_id=' + @model.get('_id').$oid
      @statistics.url = '/pitches/update_statistics?pitch_id=' + @model.get('_id').$oid
      @lists.url = '/pitches/update_lists?pitch_id=' + @model.get('_id').$oid
      @endorses.url = '/pitches/update_endorses?pitch_id=' + @model.get('_id').$oid
      @model.url = "/pitches/#{@model.get('_id').$oid }"
      @endorses.save()
    @links.save()
    @statistics.save()
    @lists.save()
    @profile.save({},success: =>
      @profile.mark_to_revert()
    )
    obj = @
    @model.set("response_form", $("#response_form_url").val())
    @model.set("redirect_to_interested_url", $("#redirect_to_interested_url").is(':checked'))
    @model.set("privacy", $("#private").is(':checked'))
    @model.save({}, {
      success:(model, response)->
        if response.success == true
          video_src = model.get("video_url")
          if video_src != ''
            html = '<embed id="video_embed" src="' + video_src  + '"type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" style="width: 305px;height: 146px"> </embed><img id="image-no-video" src="' + Studentconnect.data.video_placeholder + '" class="hide">'
          else
            html = '<embed id="video_embed" src="' + video_src  + '"type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" style="width: 0;height: 0"> </embed><img src="' + Studentconnect.data.video_placeholder  + '">'
          video_wrapper = $("body").find("#video_wrapper")
          video_wrapper.html(html)
        window.location.href = "/#pitches/edit/#{model.get('_id').$oid }" if edit
        btn_save_change.val('Save Changes')
      }
    )

    e.preventDefault()


