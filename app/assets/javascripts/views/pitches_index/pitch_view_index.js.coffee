class Studentconnect.Views.PitchView extends Backbone.View
  el: "#pitch-manager .sites"
  template: JST['pitches/pitch_li']
  initialize: ->
  render: ->
    @$el.prepend(@template({model: @model}))
    @initTooltip()
    @

  initTooltip:() ->
    $(".tip").qtip(
      show: 'mouseover',
      hide: 'mouseout'
      content:
        text: (event, api) ->
          return $(this).attr('qtip-content')
      ,
      position:
        my: 'bottom center'
        at: 'top center'
    )

