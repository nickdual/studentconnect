class Studentconnect.Views.PitchesIndex extends Backbone.View
  el: '#pitch_container'
  template: JST['pitches/index']

  initialize: ->
    @collection.on('add',@addOne, @)
    @render()
  addOne: (model) ->
    view = new  Studentconnect.Views.PitchView({model: model})
    view.render()

  addAll: ->
    @collection.forEach(@addOne, @)

  render: ->
    @$el.html(@template())
    @

