class Studentconnect.Views.PitchesManage extends Backbone.View
  el: '#pitch_container'
  template: JST['pitches/index']
  events:
    "click #check_url": "check_url"
    "click #view_contacts": "render_contacts"
    "keydown input[name='pitch[url]']": "limit_text"


  initialize: ->
    @model.on('change', @render, @)
    console.log(@model)

  render: ->
    @$el.empty()
    @$el.html(@template({model: @model}))
    unless @model.get('_id')
      @$("#work_profile").addClass('disabled')
      @$("#view_profile").addClass('disabled')
      @$("#check_url").popover(
        html: false
        placement: "right"
        content: "Please save url tag"
      )
        .popover('show')
    else
      @$("#work_profile").removeClass('disabled')
      @$("#view_profile").removeClass('disabled')
    @


  render_contacts: () ->
    @initContactPaginate()
  limit_text: (event) ->
    unless (event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 65 && event.keyCode <= 90) || (event.keyCode == 190) || (event.keyCode == 8)|| (event.keyCode == 37) || (event.keyCode == 39) || (event.keyCode == 46)
      event.preventDefault()

  check_url: (e) ->
    e.preventDefault()
    btn = $(e.currentTarget)
    value = @$("input[name='pitch[url]']").val()
    data = {}
    data.url = value
    data.pitch_id = @model.get('_id').$oid if @model.get('_id')
    btn.button('loading')
    @model.check_url({data, success: (model, response) =>
      console.log(response)
      @model.set(model)
      btn.button('reset')
      $("#url_error").html("").hide()
    ,error: (response) ->
      console.log(response)
      $("#url_error").html(response.responseText).show()
      btn.button('reset')
    })

  initContactPaginate: () ->
    @contacts = new Studentconnect.Collections.ContactsPaginate()
    view = new Studentconnect.Views.ContactsPagination({collection: @contacts})
    @contacts.fetch_with_params
      reset: true,
      success: =>
        #console.log(@contacts)





