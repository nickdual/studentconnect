class User
  include Mongoid::Document
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  has_one :pitch
  has_many :pictures
  has_many :authentications
  has_many :contacts
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable,:confirmable

  ## Database authenticatable
  field :email,              :type => String, :default => ""
  field :encrypted_password, :type => String, :default => ""
  
  ## Recoverable
  field :reset_password_token,   :type => String
  field :reset_password_sent_at, :type => Time

  ## Rememberable
  field :remember_created_at, :type => Time

  ## Trackable
  field :sign_in_count,      :type => Integer, :default => 0
  field :current_sign_in_at, :type => Time
  field :last_sign_in_at,    :type => Time
  field :current_sign_in_ip, :type => String
  field :last_sign_in_ip,    :type => String

  ## Confirmable
  field :confirmation_token,   :type => String
  field :confirmed_at,         :type => Time
  field :confirmation_sent_at, :type => Time
  field :unconfirmed_email,    :type => String # Only if using reconfirmable

  ## Lockable
  # field :failed_attempts, :type => Integer, :default => 0 # Only if lock strategy is :failed_attempts
  # field :unlock_token,    :type => String # Only if unlock strategy is :email or :both
  # field :locked_at,       :type => Time

  ## Token authenticatable
  # field :authentication_token, :type => String
  field :image_url, :type => String
  field :first_name, :type => String
  field :last_name, :type => String
  field :degree_program,              :type => String, :default => ""
  field :major,              :type => String, :default => ""
  field :college,              :type => String, :default => ""
  field :picture_id, :type => String
  #validates_uniqueness_of :first_name, scope: :last_name
  #validates_uniqueness_of :last_name, scope: :first_name

  def current_picture
    if self.picture_id
      Picture.find(self.picture_id).image.thumb.url
    else
      self.image_url
    end
  end

  def serializable_hash(options={})
    super(options.reverse_merge(:methods => [:current_picture], :except => :image))
  end

  def apply_omniauth(omni)
    authentications.build(:provider => omni['provider'],
                          :uid => omni['uid'],
                          :token => omni['credentials'].token,
                          :token_secret => omni['credentials'].secret)
  end

  def self.find_by_email(email)
    return self.where('email'=> email).first
  end
end
