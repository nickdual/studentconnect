class Photo
  include Mongoid::Document
  field :url, :type => String
  field :image, :type => String
  field :image_type, :type => String
  mount_uploader :image, PhotoUploader
  def thumb
    self.image.thumb.to_s
  end

  def serializable_hash(options={})
    super(options.reverse_merge(:methods => [:thumb], :except => :image))
  end
end
