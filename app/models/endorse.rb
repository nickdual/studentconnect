class Endorse
  include Mongoid::Document
  include Mongoid::Timestamps::Created
  field :body, type: String
  field :type, type: String
  field :show, type: Boolean, :default => true
  field :approve, type: Boolean
  belongs_to :pitch
  belongs_to :contact


end
