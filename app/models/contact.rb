class Contact
  include Mongoid::Document
  field :first_name, type: String
  field :last_name, type: String
  field :university, type: String
  field :email, type: String
  field :phone, type: String
  field :image_url, type: String
  field :type, type: String

  belongs_to :user
  has_many :endorses

end
