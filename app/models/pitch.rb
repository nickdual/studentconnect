class Pitch

  include Mongoid::Document
  included Moped::BSON::ObjectId
  field :url,              :type => String, :default => ""    #limit 100 chars
  field :title,              :type => String, :default => ""    #limit 100 chars
  field :headline,              :type => String, :default => ""    #limit 100 chars
  field :background_url,              :type => String, :default => ""    #limit 100 charsurl
  field :body_title,              :type => String, :default => ""    #limit 180charsurl
  field :body_detail,              :type => String, :default => ""    #limit 700 charsurl

  field :published,   :type => Boolean

  field :facebook_like,   :type => Boolean
  field :facebook_like_title,   :type => String

  field :endorser,   :type => Boolean

  field :statistic,   :type => Boolean


  field :share,   :type => Boolean
  field :share_title,   :type => String, :default => 'Social Networking'
  field :facebook_share , :type => Boolean ,:default => true
  field :twitter_share , :type => Boolean ,:default => true
  field :linkedIn_share , :type => Boolean ,:default => true
  field :email_share , :type => Boolean    ,:default => true

  field :video,   :type => Boolean
  field :video_title,   :type => String , :default => 'Video Title'
  field :video_url,   :type => String

  field :link,   :type => Boolean
  field :link_title,   :type => String  , :default => 'My Links'

  field :list,   :type => Boolean      , :default => 'My Lists'
  field :list_title,   :type => String
  field :privacy , :type => Boolean
  field :button_color , :type => String , :default => 'color-1'
  field :contact_btn_text ,:type => String, :default => 'Contact me'
  field :contact_mini_btn_text ,:type => String, :default => 'Get in Touch'
  field :hide_small_button , :type => Boolean , :default => false
  field :response_form , :type => String
  field :redirect_to_interested_url , :type => Boolean, :default => false
  validates_uniqueness_of :url
  has_many :statistics
  has_many :links
  has_many :lists
  belongs_to :background
  belongs_to :user
  has_many :endorses

  def update_or_build_links(array_link)
    ids = []
    array_link.each do |link|
      if link['_id']
        id = link['_id']['$oid']
        if id
          ids << id
          @link = self.links.find(id)
          if !@link.update_attributes(:title => link['title'], :url => link['url'])
            return false
          end
        end
      else
        @link = self.links.build(link)
        if @link.save()
          ids << @link.id
        else
          return false
        end
      end
    end
    Link.not_in(id: ids).destroy_all() if Link.not_in(id: ids)
    return true
  end
  def update_or_build_statistics(array_statistic)
    ids = []
    array_statistic.each do |statistic|
      if statistic['_id']
        id = statistic['_id']['$oid']
        if id
          ids << id
          @statistic = self.statistics.find(id)
          if !@statistic.update_attributes(:title => statistic['title'], :number => statistic['number'])
            return false
          end
        end
      else
        @statistic = self.statistics.build(statistic)
        if @statistic.save()
          ids << @statistic.id
        else
          return false
        end
      end
    end
    Statistic.not_in(id: ids).destroy_all() if Statistic.not_in(id: ids)
    return true
  end

  def update_or_build_lists(array_link)
    ids = []
    array_link.each do |link|
      puts link
      puts link[:list]

      if link['_id']

        id = link['_id']['$oid']
        if id
          ids << id
          @list = self.lists.find(id)
          if !@list.update_attributes(:title => link['title'], :body => link['body'], :photo_id => link['photo_id'])
            return false
          end
        end
      else
        @list = self.lists.build(link)
        if @list.save()
          ids << @list.id
        else
          return false
        end
      end
    end
    List.not_in(id: ids).destroy_all() if List.not_in(id: ids)
    return true
  end



end

