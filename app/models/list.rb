class List
  include Mongoid::Document
  field :title, type: String
  field :body, type: String
  belongs_to :pitch
  belongs_to :photo
end
