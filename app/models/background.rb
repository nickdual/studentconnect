class Background
  include Mongoid::Document
  field :url, :type => String
  field :image, :type => String
  field :image_type, :type => String
  mount_uploader :image, BackgroundUploader
  belongs_to :user

  def url
    self.image.url.to_s
  end
  def thumb
    self.image.thumb.to_s
  end

  def serializable_hash(options={})
    super(options.reverse_merge(:methods => [:thumb,:url], :except => :image))
  end
end
