class Statistic
  include Mongoid::Document
  field :title,   :type => String
  field :number,   :type => String
  belongs_to :pitch
end
