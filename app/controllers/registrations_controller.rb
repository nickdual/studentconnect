class RegistrationsController < Devise::RegistrationsController
  def create
    @user = User.new(user_params)
    if  @user.save
      sign_in(@user)
      flash[:notice] = t("users.confirmationsent")
      render :json => {:status => '200'}
      puts 'create'

    else
      render :json => {:message => @user.errors.full_messages[0].to_s ,:status => '422'}
    end
  end

  private
  def user_params
    params.require(:user).permit(:email, :first_name, :last_name, :password, :password_confirmation)
  end
end

