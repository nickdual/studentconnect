class UsersController < ApplicationController
  before_action :authenticate_login, :except => [:show,:index]

  def authenticate_login
    redirect_to '/users#sign_in' unless user_signed_in?
  end

  def index

  end

  def show

  end

  def profile
    if current_user.update_attributes(profile_params)
      render :json => {},:status => :ok
    end
  end

  def contacts
    if current_user
      @pitch = current_user.pitch
      if @pitch
        @endorses = @pitch.endorses.desc(:created_at).paginate(:page => params[:page], :per_page => params[:per_page])
        render :json => {:objects => @endorses.as_json(include: {contact: {}}), :total_pages => @pitch.endorses.count}
      else
        render :json => {:objects => nil, :total_pages => nil}
      end

    end
  end

  def unique
    if current_user
      current_user.first_name = params[:first_name]
      current_user.last_name = params[:last_name]
      if current_user.valid?
        render :json => {}, :status => :ok
      else
        render :json => {}, :status => 422
      end

    end
  end

  def contact
    if current_user
      @contact =  Contact.find(params[:id])
      render :json => @contact.to_json, :status => :ok
    end
  end

  private

  def profile_params
    params.permit(:degree_program, :major,:college,:last_name,:first_name)
  end
end
