class HomeController < ApplicationController
  #before_action :authenticate_user!
  def index
    if current_user
      @background_default = Background.limit(13)
      @pitch = current_user.pitch

    else
      redirect_to '/users#sign_in'
    end
  end
end
