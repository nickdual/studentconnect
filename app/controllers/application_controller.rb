class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :configure_permitted_parameters, if: :devise_controller?
  skip_before_filter  :verify_authenticity_token
  #before_action :authenticate_user!

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:username, :email,:first_name, :last_name) }
  end

  def after_sign_in_path_for(resource_or_scope)
    "/#profileMgr"
  end

  def after_confirmation_path_for(resource_name, resource)
    "/users#login"
  end
  def after_resending_confirmation_instructions_path_for(resource_name)
    "/users#login"
  end


end
