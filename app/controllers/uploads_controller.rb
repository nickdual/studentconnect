class UploadsController < ApplicationController
  def background
    @background = Background.new(params.require(:background).permit(:image))
    if @background.save
      render :json => @background
    end
  end

  def picture
    @picture = Picture.new(params.require(:picture).permit(:image))
    @picture.user_id = current_user.id if current_user
    if @picture.save
      current_user.picture_id = @picture.id
      current_user.save
      render :json => @picture
    end
  end

  def photo
    @photo = Photo.new(params.require(:photo).permit(:image))
    if @photo.save
      render :json => @photo
    end
  end
end
