class SessionsController < Devise::SessionsController

  def destroy
    super

  end

  def create
    @user =  User.where(email: params[:user][:email]).first
    if !@user.present?
      render :json => {:success => false, :flag => 'email_error',:message => "Email or password not correct"}
    else
      resource = warden.authenticate!(:scope => resource_name ,:recall => "sessions#failure")
      puts resource
      set_flash_message(:notice, :signed_in) if is_navigational_format?
      sign_in(resource_name, resource)
      if signed_in?
        render :json =>  {:success => true, :user => resource}
      else
        render :json => {:success => false, :flag => 'password_error',:message => "Email or password not correct"}
      end
    end
  end

end



