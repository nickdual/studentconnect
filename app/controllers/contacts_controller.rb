class ContactsController < ApplicationController
  def index
    @contacts = nil
    key_search =  params[:key_search]
    if key_search.present?
      @contacts = current_user.contacts.where('$or' => [ {:first_name => /#{Regexp.escape(key_search)}/i},{:last_name => /#{Regexp.escape(key_search)}/i}, {:email => /#{Regexp.escape(key_search)}/i} ]).limit(5)
    else
      @contacts = current_user.contacts
    end

    render :json => @contacts
  end
  def create
    if current_user
      @contact = current_user.contacts.build(params[:contact].permit(:first_name, :last_name, :university, :email))
      if @contact.save
        render :json => @contact
      end
    end
  end
  def endorse
    @endorse = Endorse.find(params[:id])
    render :json => @endorse.to_json(include: {contact: {},pitch: {only: [:url,:headline]}})  if @endorse
  end

  def add_contact
    if params[:type] == "endorse"
      @endorse = add_endorse(params)
      render :json => @endorse.to_json(include: {contact: {}})
      return
    else
      @user = current_user
      email_contact =  params[:contact].split(',')
      email_contact.each do |email|
        user = User.find_by_email(email)
        if user
          image_url = user.current_picture
          @contact = current_user.contacts.build('email'=>user.email, 'first_name'=> user.first_name, "last_name" => user.last_name,'image_url'=> image_url)
          @contact.save
        else
          @contact = current_user.contacts.build('email'=>email,'image_url'=> "/assets/default_picture.png")
          @contact.save
        end

      end
      render :json => @user.contacts
      return
    end

  end

  def get_contacts
    @user = current_user
    key_search = params[:key_search]
    @contacts = []
    if key_search.present?
      @contacts = @user.contacts.where('$or' => [ {:first_name => /#{Regexp.escape(key_search)}/i},{:last_name => /#{Regexp.escape(key_search)}/i}, {:email => /#{Regexp.escape(key_search)}/i} ]).limit(5)
    end
    respond_to do |format|
      format.json { render :json => @contacts }
    end
  end

  private
  def add_endorse(data)
    @pitch = Pitch.find(data[:pitch_id])
    @contact = Contact.where({email: data[:contact][:email], type: data[:endorse][:type]}).first
    if @contact
      #if data[:endorse][:type] == "endorse"
      #  @contact.first_name = data[:contact][:first_name]
      #  @contact.last_name = data[:contact][:last_name]
      #  @contact.type = data[:endorse][:type]
      #end
      #@contact.phone = data[:contact][:phone]
    else
      @contact = Contact.new(data[:contact].permit(:first_name, :last_name, :university, :email,:phone))
      @contact.user_id = @pitch.user_id
      @contact.type = data[:endorse][:type]
    end


    @endorse = Endorse.new({body: data[:endorse][:body], type: data[:endorse][:type]})
    if @contact.save
      @endorse.contact_id = @contact.id
      @endorse.pitch_id = @pitch.id
      if @endorse.save
        #send mail
        if data[:endorse][:type] == "contact"
          data_info = {}
          data_info[:email] = @pitch.user.email
          data_info[:first_name] = @contact.first_name
          data_info[:last_name] = @contact.last_name
          data_info[:profile] = "your Profile"
          data_info[:subject] = (@contact.new_record? ? "You have received a response from a new contact on your Profile" : "You have received a response from a existing contact on your Profile" )
          data_info[:link] = "http://#{request.host_with_port}/#responses/#{@endorse.id}"
          User_Mailer.contact_email(data_info).deliver
        end
      end

    end
    @endorse
  end

  def contact_params(data)
    data.permit(:first_name, :last_name, :university, :email,:phone)
  end

end
