class MailersController < ApplicationController
  def invite_email()
    data_info = params[:info]
    if current_user
      data_info[:link] = "http://#{request.host}/#{current_user.pitch.url}"
    end
    User_Mailer.invite_email(data_info).deliver
    render :text => :ok
  end

end
