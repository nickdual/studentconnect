class PitchesController < ApplicationController
  before_action :authenticate_login, :except => [:show]

  def authenticate_login
    unless user_signed_in?
      redirect_to '/users#sign_in'
    end
  end

  def index
    @pitch = current_user.pitch
    render :json => @pitch
  end

  def show
    if params[:url]
      @pitch = Pitch.where(url: params[:url],published: true).first
      if @pitch.nil?
        redirect_to '/'
      end
    else
      if params[:id]
        @pitch = Pitch.find(params[:id])
        render :json => @pitch.to_json(include: {user: {}})
      end
    end
  end

  def edit
    @pitch = Pitch.where(:url => params[:url]).first
    @background_default = Background.limit(13)
    #render :json => @pitch.to_json(include: {user: {include: {profile: {} }}})
  end

  def create
    @pitch = Pitch.new(pitch_params)
    @pitch.user_id = current_user.id  if current_user
    if @pitch.save
      render :json => @pitch
    else
      render :json => nil
    end
  end

  def update
    @pitch = Pitch.find(params[:id])
    if @pitch.update_attributes(pitch_params)
      render :json => {success: true, pitch: @pitch}
    else
      render :json => {success: false, pitch: nil}
    end
  end

  def delete
  end

  def new
  end
  def link

  end

  def approve
    @endorse = Endorse.find(params[:id])
    if @endorse
      @endorse.approve = true
      if @endorse.save
         render :json => {}, :status  => :ok
      else
        render :json => {}, :status => 422
      end
    end

  end

  def update_published
    @pitch = Pitch.find(params[:id])
    pub = params[:published] == "true" ? true : false
    if @pitch
      if @pitch.update_attribute('published', pub)
        render :json => {success: true}
      else
        render :json => {success: false}
      end

    end
  end

  def links
    @links = nil
    if params[:pitch_id].present?
      @pitch = Pitch.find(params[:pitch_id])
      @links = @pitch.links
    end
    respond_to do |format|
      format.json { render json: @links }
    end
    #render :json => @links
  end
  def statistics
    @statistics = nil
    if params[:pitch_id].present?
      @pitch = Pitch.find(params[:pitch_id])
      @statistics = @pitch.statistics
    end
    respond_to do |format|
      format.json { render json: @statistics }
    end
  end

  def update_links
    @pitch = Pitch.find(params[:pitch_id])
    if params[:links]
      @result = @pitch.update_or_build_links(params[:links])
      if @result == true
        render :json => true
      else
        render :json => false
      end
    else
      @pitch.links = []
      if @pitch.save
        render :json => true
      end
    end
  end
  def update_statistics
    @pitch = Pitch.find(params[:pitch_id])
    if params[:statistics]
      @result = @pitch.update_or_build_statistics(params[:statistics])
      if @result == true
        render :json => true
      else
        render :json => false
      end
    else
      @pitch.statistics = []
      if @pitch.save
        render :json => true
      end
    end
  end

  def list_delete_photo
    @list = List.find(params[:list_id])
      if @list.photo.nil?
        render :json => "ok"
      else
        @list.photo.destroy
        @list.photo_id = nil
        @list.save
        render :json => @list

      end
  end

  def endorses
    @endorses = nil
    query = {}
    query[:approve]= params[:approve] if params[:approve]
    query[:show] = params[:show] if params[:show]
    query[:type] = params[:type] if params[:type]

    puts query
    if params[:pitch_id]
      @pitch = Pitch.find(params[:pitch_id] )
      @endorses = @pitch.endorses.where(query).desc(:created_at)
    else
      @pitch = current_user.pitch if current_user
      @endorses = @pitch.endorses.where(query).desc(:created_at)
    end

    render  :json => @endorses.to_json(include: {contact: {},pitch: {only: [:url,:headline]}})
  end

  def update_endorses
    if change_show_endorses(params[:endorses])
       render :json => "Success"
    else
      render :json => "Error"
    end

  end
  def lists
    @lists = nil
    if params[:pitch_id]
      @pitch = Pitch.find(params[:pitch_id])
      @lists = @pitch.lists
    end
    render  :json => @lists.to_json(include: {photo: {}})
  end

  def update_lists
    @pitch = Pitch.find(params[:pitch_id])
    if params[:lists]
      @result = @pitch.update_or_build_lists(params[:lists])
      if @result == true
        render :json => true
      else
        render :json => false
      end
    else
      @pitch.lists = []
      if @pitch.save
        render :json => true
      end
    end
  end

  def endorsement
    render :json => :ok
  end
  def promote
      render :json => :ok
  end

  def invite
    render :text => 'invite page'
  end

  def check_url
    @pitch = Pitch.where(:url => params[:url].strip, :user_id.ne => current_user.id)
    if @pitch.length > 0
      render :json => "This take is taken. Please select another.", :status => :unprocessable_entity
    else
      if params[:pitch_id]
        @pitch = Pitch.find(params[:pitch_id])
        @pitch.url = params[:url].strip
        @pitch.save()
      else
        @pitch = Pitch.new({
          title: "Title",
          headline: "This is where your headline goes",
          link_title: "My Links",
          list_title: "Additional Lists Title",
          facebook_like_title: "LIKE THIS PITCH",
          share_title: "SHARE THIS PITCH WITH OTHERS",
          video_title: "My details",
          contact_btn_text: "Contact me",
          contact_mini_btn_text: "Get in Touch",
          background_url: "",
          body_title: "Site Manager",
          body_detail: "Tell your story here. Say why you got started, why you love it, how you're different from others in your market, etc. You want people to get your story right away. Be authentic. Explain it like you're talking to your mom.",
          published: true,
          facebook_like: true,
          endorser: true,
          statistic: true,
          share: true,
          facebook_share: true,
          twitter_share: true,
          linkedIn_share: true,
          email_share: true,
          video: true,
          link: true,
          list: true,
          hide_small_button: false,
          redirect_to_interested_url: false,
          user_id: current_user.id,
          url: params[:url].strip
        })
        @pitch.save()
      end
      render :json => @pitch.to_json, :status => :ok
    end
  end

  private
  def pitch_params
    params[:pitch].delete :_id
    params[:pitch].delete :success
    params[:pitch].delete :user_id
    params[:pitch].delete :pitch
    params.require(:pitch).permit(:title, :headline, :background_url, :body_title, :body_detail, :background_id, :endorser, :facebook_like, :facebook_like_title, :share, :share_title, :facebook_share ,:twitter_share ,:linkedIn_share ,:email_share ,:statistic, :link_about, :link_blog, :link_portfolio, :list, :list_items, :list_title, :published, :user_id, :video, :video_title, :video_url, :link, :link_item, :link_title, :privacy, :button_color,:contact_btn_text,:contact_mini_btn_text,:hide_small_button, :response_form, :redirect_to_interested_url,:privacy)
  end

  def change_show_endorses(array_endorses)
    unless array_endorses.nil?
      array_endorses.each do |endorse|

        if endorse['_id']

          id = endorse['_id']['$oid']
          if id
            @endorse = Endorse.find(id)
            if !@endorse.update_attributes(:show => endorse['show'])
              return false
            end
          end
        end
      end
    end
    return true
  end
end
