class User_Mailer < ActionMailer::Base
  default :from => "example@gmail.com",
          "Reply-To" => "reply-to@deekau.com"

  def invite_email(params)
    @params = params
    @body = @params[:body]
    @link = @params[:link]
    mail(to: @params[:email], subject: @params[:subject] )
  end

  def contact_email(params)
    @first_name =  params[:first_name]
    @last_name =  params[:last_name]
    @profile = params[:profile]
    @link = params[:link]
    mail(to: params[:email], subject: params[:subject] )
  end

end
