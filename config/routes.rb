Studentconnect::Application.routes.draw do
  post "users/profile"
  put "users/profile"
  post "uploads/background"
  post "uploads/picture"
  post "uploads/photo"
  post "/pitches/update_published"
  get 'pitches/links'
  get 'pitches/statistics'
  put 'pitches/links'
  put 'pitches/statistics'

  put 'pitches/update_links'
  put 'pitches/update_statistics'
  put 'pitches/update_endorses'
  get 'pitches/lists'
  put 'pitches/lists'
  put 'pitches/update_lists'
  post 'pitches/list_delete_photo'
  get '/pitches/endorsement'
  get '/pitches/invite'

  resource :contacts do
    collection do
      post :add_contact
      get :get_contacts
      get :index
      get :endorse
    end
  end

  resources :pitches do
    collection do
      get :endorses
      post :check_url
      post :approve
    end
  end

  resources :mailers do
    collection do
      post :invite_email
    end
  end
  devise_for :users ,  controllers: {:omniauth_callbacks => "omniauth_callbacks", registrations: "registrations", sessions: "sessions", passwords: "passwords"}
  resources :users do
    collection do
      get 'contacts'
      get 'contact'
      post 'unique'
    end
  end
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'home#index'

  # Example of regular route:
  get '/:url/edit' => 'pitches#edit',:constraints => { :url => /.*/ }
  get '/:url' => 'pitches#show',:constraints => { :url => /.*/ }

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end
  
  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
